@extends('templates.dark.main')

{{--Attributes for Layout are added here --}}
@section('body-classes','single-column')

@section('content')

    @include('demo-content.single-column')

@endsection

