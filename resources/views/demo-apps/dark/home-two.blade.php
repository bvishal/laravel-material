@extends('templates.dark.main')

@section('content')

    <div class="card">
        <div class="card-body">
            <h4 class="font-weight-bold py-3 mb-4">Home Two</h4>
            <p>This page is the second example of the basic layout to get you started.</p>
            <p><button class="btn btn-primary btn-lg">Button</button></p>
        </div>
    </div>

@endsection
