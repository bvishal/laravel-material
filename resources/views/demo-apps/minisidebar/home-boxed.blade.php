@extends('templates.minisidebar.main')

{{--Attributes for Layout are added here --}}
@section('body-classes','boxed')

@section('content')

    @include('demo-content.home-boxed')

@endsection

