@extends('templates.minisidebar.main')

{{--Attributes for Layout are added here --}}
@section('body-classes','fix-header fix-sidebar ')

@section('content')

    @include('demo-content.fix-header-sidebar')

@endsection

