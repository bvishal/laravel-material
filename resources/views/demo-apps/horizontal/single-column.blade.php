@extends('templates.horizontal.main')

{{--Attributes for Layout are added here --}}
@section('body-classes','fix-header fix-sidebar card-no-border logo-center')

{{--Attributes for Layout are added here --}}
@section('body-classes','single-column')

@section('content')

    @include('demo-content.single-column')

@endsection

