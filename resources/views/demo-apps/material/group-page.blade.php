@extends('templates.material.main')

{{--Attributes for Layout are added here --}}
@section('body-classes','fix-header')

@section('content')

    @includeFirst(["demo-content.groups.{$group}.{$page}", 'demo-content.view-not-found-blade'])

@endsection

