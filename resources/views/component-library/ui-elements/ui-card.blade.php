<div class="card {{ $class or '' }}  {{ $text_align or '' }} {{ $card_width or '' }}">

    @isset($url)
        <img class="card-img-top img-responsive" src="{{ $url }}" alt="{{ $alt }}">
    @endisset

    @isset($header)
            <div class="card-header">

            @if(isset($class))
                    <h4 class="m-b-0 text-white">{{ $header }}</h4>
            @else
                    {{ $header }}
            @endif

            @isset($has_actions)
                <div class="card-actions">
                    <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                    <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
                </div>
            @endisset
            </div>
    @endisset

    @if(isset($title) || isset($subtitle) || isset($text) || isset($button_text) || isset($action) || isset($button_class) || $slot !== '')
        <div class="card-body">
            @isset($title)
                <h4 class="card-title">{{ $title }}</h4>
            @endisset

            @isset($subtitle)
                <h4 class="card-subtitle ">{{ $subtitle }}</h4>
            @endisset

            @isset($text)
                <p class="card-text">{{ $text }}</p>
            @endisset

            {{ $slot }}

            @isset($action, $button_text)
                <a href="{{ $action }}" class="btn {{ isset($button_class) ? $button_class : 'btn-primary' }}">{{ $button_text }}</a>
            @endisset
        </div>
    @endif

    @isset($footer)
        <div class="card-footer">{{ $footer }}</div>
    @endisset

</div>