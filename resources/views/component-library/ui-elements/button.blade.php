<button
        type="button"

        @isset($data_toggle)            data-toggle="{{ $data_toggle }}"                    @endisset
        @isset($data_placement)         data-placement="{{ $data_toggle }}"                 @endisset
        @isset($title)                  title="{{ $title }}"                                @endisset
        @isset($data_more)              data-more="{{ $data_more }}"                        @endisset
        @isset($aria_pressed)           aria-pressed="{{ $aria_pressed }}"                  @endisset
        @isset($data_container)         data-container="{{ $data_container }}"              @endisset
        @isset($data_content)           data-content="{{ $data_content }}"                  @endisset
        @isset($data_original_title)    data-original_title="{{ $data_original_title }}"    @endisset

        class="btn
            {{ $color or ' btn-primary ' }}
            @isset($size)     @if($size)     {{ $size }}     @endif @endisset
            @isset($rounded)  @if($rounded)  ' btn-rounded ' @endif @endisset
            @isset($circle)   @if($circle)   ' btn-rounded ' @endif @endisset
            @isset($disabled) @if($disabled) ' disabled '    @endif @endisset
            @isset($block)    @if($block)    ' btn-block '   @endif @endisset
            waves-effect
            waves-light"
>
    
    {{ $icon or '' }}

    {{ $text or '' }}

</button>