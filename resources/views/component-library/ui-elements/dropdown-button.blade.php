<button
        type="button"
        id=" {{ $id or '' }}"
        class="btn {{ $color or 'btn-primary' }} dropdown-toggle dropdown-toggle-split"
        data-toggle="dropdown"
        aria-haspopup="true"
        aria-expanded="false"
>
    {{ $icon or '' }}

    <span class="sr-only">{{ $text or '' }}</span>

    {{ $sr_text or '' }}
</button>