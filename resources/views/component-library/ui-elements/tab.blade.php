<ul class="nav nav-tabs {{ $class or '' }}" role="tablist">

    @forelse($tab->tabPanes as $tabPane)

        <li class="nav-item">
            <a class="nav-link  @if ($loop->first) active @endif "
               data-toggle="tab"
               href="#{{ $tabPane->domId }}"
               role="tab"
            >
                <span class="hidden-sm-up"> <i class="ti-home"></i></span>
                <span class="hidden-xs-down">{{ $tabPane->title }}</span>
            </a>
        </li>

    @empty
        No panes here!
    @endforelse

</ul>

<!-- Tab panes -->
<div class="tab-content @isset($tabContentBorder) @if($tabContentBorder) ' tabcontent-border ' @endif @endisset">

    @forelse($tab->tabPanes as $tabPane)

        <div class="tab-pane @if ($loop->first) active @endif " id="{{ $tabPane->domId }}" role="tabpanel">

            <div class="p-20">
                {!! $tabPane->content !!}
            </div>

        </div>

    @empty
        No panes here!
    @endforelse
</div>


