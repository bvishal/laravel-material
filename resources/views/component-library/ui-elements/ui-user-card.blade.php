<div class="card">
    <div class="el-card-item">
        <div class="el-card-avatar el-overlay-1">
            @isset($url)
            <img src="{{ $url }}" alt="{{ $alt }}" />
            @endisset
            <div class="el-overlay {{ $effect or '' }}">
                <ul class="el-info">
                    <li>
                        <a class="btn default btn-outline image-popup-vertical-fit" href="{{ $action }}">
                            <i class="icon-magnifier"></i>
                        </a>
                    </li>
                    <li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="el-card-content">
            @isset($title)
            <h3 class="box-title">{{ $title }}</h3> <small>{{ $subtitle or '' }}</small>
            @endisset
        </div>
    </div>
</div>