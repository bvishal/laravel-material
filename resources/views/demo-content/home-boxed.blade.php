<div class="card">
    <div class="card-body">
        <h4 class="font-weight-bold py-3 mb-4">Boxed</h4>
        <p>This page is an example of boxed layout to get you started.</p>
        <p><button class="btn btn-primary btn-lg">Button</button></p>
    </div>
</div>

@include('demo-content.card-long-text')