<div class="card">
    <div class="card-body">
        <h3 class="font-weight-bold py-3 mb-4">Fixed Sidebar</h3>

        <h4 class="font-weight-bold py-3 mb-4">
            <p>Make sure the browser window height is short enough so that the sidebar menu items don't all fit.</p>
            <p>Then you will see scroll possibility on the sidebar</p>
        </h4>

    </div>
</div>

@include('demo-content.card-long-text')