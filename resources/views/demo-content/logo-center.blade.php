<div class="card">
    <div class="card-body">
        <h4 class="font-weight-bold py-3 mb-4">Logo Center</h4>
        <p>This page is an example of logo center layout to get you started.</p>
        <p><button class="btn btn-primary btn-lg">Button</button></p>
    </div>
</div>

@include('demo-content.card-long-text')