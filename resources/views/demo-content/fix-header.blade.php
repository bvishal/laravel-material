<div class="card">
    <div class="card-body">

        <h3 class="font-weight-bold py-3 mb-4">Fixed Header</h3>

        <h4 class="font-weight-bold py-3 mb-4">
            <p>Long page, so you can scroll and check the fixed header</p>
        </h4>

    </div>
</div>

@include('demo-content.card-long-text')
