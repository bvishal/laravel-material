@push('before-styles')

    <!-- Editable CSS -->
    <link type="text/css" rel="stylesheet" href="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/jsgrid/jsgrid.min.css" />
    <link type="text/css" rel="stylesheet" href="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/jsgrid/jsgrid-theme.min.css" />

@endpush

@push('after-scripts')

    <!-- Editable -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/jsgrid/db.js"></script>
    <script type="text/javascript" src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/jsgrid/jsgrid.min.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/material/js/jsgrid-init.js"></script>

@endpush

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-12">
        <!-- Column -->
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Editable with Datatable</h4>
                <div id="basicgrid"></div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Static</h4>
                <div id="staticgrid"></div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Soarting</h4>
                <div class="col-md-2 row">
                    <select id="sortingField" class="custom-select form-control input-sm m-b-10">
                        <option>Name</option>
                        <option>Age</option>
                        <option>Address</option>
                        <option>Country</option>
                        <option>Married</option>
                    </select>
                </div>
                <div id="exampleSorting"></div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="card">
            <div class="card-body">
                <h4 class="card-title"></h4>
                <h6 class="card-subtitle"></h6>
            </div>
        </div>
        <!-- Column -->
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->