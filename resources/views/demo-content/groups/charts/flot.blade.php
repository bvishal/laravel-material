@push('after-scripts')

    <!-- Flot Charts JavaScript -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/flot/excanvas.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/flot/jquery.flot.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/flot/jquery.flot.pie.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/flot/jquery.flot.time.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/flot/jquery.flot.stack.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/flot/jquery.flot.crosshair.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/material/js/flot-data.js"></script>

@endpush

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <!-- column -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Real Chart</h4>
                <div class="demo-container" style="height:400px;">
                    <div id="placeholder" class="flot-chart-content"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- column -->
    <!-- column -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Line Chart</h4>
                <div class="flot-chart">
                    <div class="flot-chart-content" id="flot-line-chart"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- column -->
    <!-- column -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Pie Chart</h4>
                <div class="flot-chart">
                    <div class="flot-chart-content" id="flot-pie-chart"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- column -->
    <!-- column -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Moving Line Chart Example</h4>
                <div class="flot-chart">
                    <div class="flot-chart-content" id="flot-line-chart-moving"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- column -->
    <!-- column -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Bar Chart</h4>
                <div class="flot-chart">
                    <div class="flot-chart-content" id="flot-bar-chart"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- column -->
    <!-- column -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Sales Bar Chart</h4>
                <div class="flot-chart">
                    <div class="sales-bars-chart" style="height: 320px;"> </div>
                </div>
            </div>
        </div>
    </div>
    <!-- column -->
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->