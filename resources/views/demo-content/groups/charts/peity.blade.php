@push('after-scripts')

    <!-- Chart JS -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/peity/jquery.peity.min.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/peity/jquery.peity.init.js"></script>

@endpush

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <!-- column -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Pie Chart</h4>
                <h6 class="card-subtitle">Add code in span <code> class="pie"</code> plus for colors <code>data-peity='{ "fill": ["#13dafe", "#f2f2f2"]}'</code></h6>
                <div class="row">
                    <div class="col-lg-2 col-md-6 m-b-30"><span class="pie" data-peity='{ "fill": ["#009efb", "#f2f2f2"]}'>1/5</span>
                        <div>Data : 1/5</div>
                    </div>
                    <div class="col-lg-2 col-md-6 m-b-30"><span class="pie" data-peity='{ "fill": ["#7460ee", "#f2f2f2"]}'>226/360</span>
                        <div>Data : 226/360</div>
                    </div>
                    <div class="col-lg-2 col-md-6 m-b-30"><span class="pie" data-peity='{ "fill": ["#f96262", "#f2f2f2"]}'>0.52/1.561</span>
                        <div>Data : 0.52/1.561</div>
                    </div>
                    <div class="col-lg-2 col-md-6 m-b-30"><span class="pie" data-peity='{ "fill": ["#26c6da", "#f2f2f2"]}'>1,4</span>
                        <div>Data : 1,4</div>
                    </div>
                    <div class="col-lg-2 col-md-6 m-b-30"><span class="pie" data-peity='{ "fill": ["#ffbc34", "#f2f2f2"]}'>226,134</span>
                        <div>Data : 226,134</div>
                    </div>
                    <div class="col-lg-2 col-md-6"><span class="pie" data-peity='{ "fill": ["#4c5667", "#f2f2f2"]}'>0.52,1.041</span>
                        <div>Data : 0.52, 1.041</div>
                    </div>
                </div>
                <div class="row m-t-40">
                    <div class="col-12">
                        <br>
                        <br>
                        <h4 class="card-title">Donut Chart</h4>
                        <h6 class="card-subtitle">Just add in span<code> class="donut"</code></h6>
                    </div>
                    <div class="col-lg-2 col-md-6 m-b-30"><span class="donut" data-peity='{ "fill": ["#009efb", "#f2f2f2"]}'>1/5</span>
                        <div>Data : 1/5</div>
                    </div>
                    <div class="col-lg-2 col-md-6 m-b-30"><span class="donut" data-peity='{ "fill": ["#7460ee", "#f2f2f2"]}'>226/360</span>
                        <div>Data : 226/360</div>
                    </div>
                    <div class="col-lg-2 col-md-6 m-b-30"><span class="donut" data-peity='{ "fill": ["#f96262", "#f2f2f2"], "innerRadius": 16, "radius": 32 }'>0.52/1.561</span>
                        <div>Data : 0.52/1.561</div>
                    </div>
                    <div class="col-lg-2 col-md-6 m-b-30"><span class="donut" data-peity='{ "fill": ["#26c6da", "#f2f2f2"], "innerRadius": 20, "radius": 32 }'>1,4</span>
                        <div>Data : 1,4</div>
                    </div>
                    <div class="col-lg-2 col-md-6 m-b-30"><span class="donut" data-peity='{ "fill": ["#ffbc34", "#f2f2f2"], "innerRadius": 23, "radius": 32 }'>226,134</span>
                        <div>Data : 226,134</div>
                    </div>
                    <div class="col-lg-2 col-md-6"><span class="donut" data-peity='{ "fill": ["#4c5667", "#f2f2f2"], "innerRadius": 8, "radius": 32 }'>0.52,1.041</span>
                        <div>Data : 0.52, 1.041</div>
                    </div>
                </div>
                <div class="row m-t-40">
                    <div class="col-12">
                        <br>
                        <br>
                        <h4 class="card-title">Bar Chart</h4>
                        <h6 class="card-subtitle">Add class in span<code> peity-bar</code></h6>
                    </div>
                    <div class="col-lg-4 m-b-30"><span class="peity-bar" data-peity='{ "fill": ["#7460ee", "#009efb"]}' data-width="100%" data-height="60">6,2,8,4,3,8,1,3,6,5,9,2,8,1,4,8,9,8,2,1</span></div>
                    <div class="col-lg-4 m-b-30"><span class="peity-bar" data-peity='{ "fill": ["#f96262", "#f2f2f2"]}' data-width="100%" data-height="60">6,2,8,4,-3,8,1,-3,6,-5,9,2,-8,1,4,8,9,8,2,1</span></div>
                    <div class="col-lg-4 m-b-30"><span class="peity-bar" data-peity='{ "fill": ["#4c5667", "#26c6da"]}' data-width="100%" data-height="60">6,2,8,4,3,8,1,3,6,5,9,2,8,1,4,8,9,8,2,1</span></div>
                </div>
                <div class="row m-t-40">
                    <div class="col-12">
                        <br>
                        <br>
                        <h4 class="card-title">Line Chart</h4>
                        <h6 class="card-subtitle">Add class in span<code> peity-line</code></h6>
                    </div>
                    <div class="col-lg-4 m-b-30"><span class="peity-line" data-width="100%" data-height="60">6,2,8,4,3,8,1,3,6,5,9,2,8,1,4,8,9,8,2,1</span></div>
                    <div class="col-lg-4 m-b-30"><span class="peity-line" data-width="100%" data-height="60">6,2,8,4,-3,8,1,-3,6,-5,9,2,-8,1,4,8,9,8,2,1</span></div>
                    <div class="col-lg-4 m-b-30"><span class="peity-line" data-width="100%" data-height="60">6,2,8,4,3,8,1,3,6,5,9,2,8,1,4,8,9,8,2,1</span></div>
                </div>
            </div>
        </div>
    </div>
    <!-- column -->
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->