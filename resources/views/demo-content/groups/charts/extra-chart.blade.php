@push('before-styles')

    <!-- Bootstrap Core CSS -->
    <link href="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/css-chart/css-chart.css" rel="stylesheet">

@endpush

@push('after-scripts')

    <!-- EASY PIE CHART JS -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/jquery.easy-pie-chart/easy-pie-chart.init.js"></script>

@endpush

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <!-- column -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Easy Pie Chart</h4>
                <h6 class="card-subtitle">Add code in span <code> class="chart easy-pie-chart-1"</code> to create chart</h6>
                <div class="row">
                    <div class="col-lg-2 col-md-6 m-b-30">
                        <div class="chart easy-pie-chart-1" data-percent="75"> <span class="percent">8/100</span> </div>
                    </div>
                    <div class="col-lg-2 col-md-6 m-b-30">
                        <div class="chart easy-pie-chart-2" data-percent="75"> <span class="percent">75</span> </div>
                    </div>
                    <div class="col-lg-2 col-md-6 m-b-30">
                        <div class="easy-pie-chart-3 chart pie-chart" data-percent="25"> <span>25.00 <br>
                                            <small class="text-muted">/100mb</small></span> </div>
                    </div>
                    <div class="col-lg-2 col-md-6 m-b-30">
                        <div class="chart easy-pie-chart-4" data-percent="75"> <span class="percent">8/100</span> </div>
                    </div>
                    <div class="col-lg-2 col-md-6 m-b-30">
                        <div class="chart easy-pie-chart-5" data-percent="75"> <span class="percent">75</span> </div>
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <div class="easy-pie-chart-6 chart pie-chart" data-percent="25"> <span>25.00 <br>
                                          <small class="text-muted">/100mb</small></span> </div>
                    </div>
                </div>
                <div class="row m-t-40">
                    <div class="col-12">
                        <br>
                        <br>
                        <h4 class="card-title">Easy Pie Avtar Chart</h4>
                        <h6 class="card-subtitle">Just add in div<code> class="chart easy-pie-chart-1"</code></h6>
                    </div>
                    <div class="col-lg-2 col-md-6 m-b-30">
                        <div class="chart easy-pie-chart-1" data-percent="75"> <span><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/1.jpg" alt="user" class="img-circle"/></span> </div>
                    </div>
                    <div class="col-lg-2 col-md-6 m-b-30">
                        <div class="chart easy-pie-chart-2" data-percent="75"> <span><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/2.jpg" alt="user"  class="img-circle"/></span> </div>
                    </div>
                    <div class="col-lg-2 col-md-6 m-b-30">
                        <div class="easy-pie-chart-3 chart" data-percent="25"> <span><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/3.jpg" alt="user" class="img-circle"/></span> </div>
                    </div>
                    <div class="col-lg-2 col-md-6 m-b-30">
                        <div class="chart easy-pie-chart-4" data-percent="75"> <span><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/4.jpg" alt="user" class="img-circle"/></span> </div>
                    </div>
                    <div class="col-lg-2 col-md-6 m-b-30">
                        <div class="chart easy-pie-chart-5" data-percent="75"> <span><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/5.jpg" alt="user" class="img-circle"/></span> </div>
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <div class="easy-pie-chart-6 chart" data-percent="25"> <span><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/6.jpg" alt="user"  class="img-circle"/></span> </div>
                    </div>
                </div>
                <div class="row m-t-40">
                    <div class="col-12">
                        <br>
                        <br>
                        <h4 class="card-title">Css Charts</h4>
                        <h6 class="card-subtitle">Just add class <code>.css-bar</code> and data lable </h6>
                    </div>
                    <div class="col-lg-3 m-b-30">
                        <div data-label="10%" class="css-bar css-bar-10 css-bar-lg css-bar-default"></div>
                    </div>
                    <div class="col-lg-3 m-b-30">
                        <div data-label="20%" class="css-bar css-bar-20 css-bar-lg"></div>
                    </div>
                    <div class="col-lg-3 m-b-30">
                        <div data-label="30%" class="css-bar css-bar-30 css-bar-lg css-bar-success"></div>
                    </div>
                    <div class="col-lg-3 m-b-30">
                        <div data-label="40%" class="css-bar css-bar-40 css-bar-lg css-bar-warning"></div>
                    </div>
                </div>
                <div class="row m-t-40">
                    <div class="col-12">
                        <br>
                        <br>
                        <h4 class="card-title">All Size</h4>
                        <h6 class="card-subtitle"><code>.css-bar-lg</code>, <code>.css-bar</code>, <code>.css-bar-sm</code>, <code>.css-bar-xs</code></h6>
                    </div>
                    <div class="col-lg-3 m-b-30">
                        <div data-label="10%" class="css-bar css-bar-10 css-bar-lg css-bar-default"></div>
                    </div>
                    <div class="col-lg-3 m-b-30">
                        <div data-label="20%" class="css-bar css-bar-20 css-bar"></div>
                    </div>
                    <div class="col-lg-3 m-b-30">
                        <div data-label="30%" class="css-bar css-bar-30 css-bar-sm css-bar-success"></div>
                    </div>
                    <div class="col-lg-3 m-b-30">
                        <div data-label="40%" class="css-bar css-bar-40 css-bar-xs css-bar-warning"></div>
                    </div>
                </div>
                <div class="row m-t-40">
                    <div class="col-12">
                        <br>
                        <br>
                        <h4 class="card-title">Chart With image</h4>
                        <h6 class="card-subtitle">put image between div</h6>
                    </div>
                    <div class="col-lg-3 m-b-30">
                        <div data-label="10%" class="css-bar css-bar-10 css-bar-lg css-bar-default"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/1.jpg" alt="User"></div>
                    </div>
                    <div class="col-lg-3 m-b-30">
                        <div data-label="20%" class="css-bar css-bar-20 css-bar-lg"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/2.jpg" alt="User"></div>
                    </div>
                    <div class="col-lg-3 m-b-30">
                        <div data-label="30%" class="css-bar css-bar-30 css-bar-lg css-bar-success"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/3.jpg" alt="User"></div>
                    </div>
                    <div class="col-lg-3 m-b-30">
                        <div data-label="40%" class="css-bar css-bar-40 css-bar-lg css-bar-warning"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/4.jpg" alt="User"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- column -->
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->