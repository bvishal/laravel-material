@push('after-scripts')

    <!-- Chart JS -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/echarts/echarts-all.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/echarts/echarts-init.js"></script>

@endpush

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <!-- column -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Monthly Report</h4>
                <div id="bar-chart" style="width:100%; height:400px;"></div>
            </div>
        </div>
    </div>
    <!-- column -->
    <!-- column -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Line chart</h4>
                <div id="main" style="width:100%; height:400px;"></div>
            </div>
        </div>
    </div>
    <!-- column -->
    <!-- column -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Monthly Salary Chart</h4>
                <div id="pie-chart" style="width:100%; height:400px;"></div>
            </div>
        </div>
    </div>
    <!-- column -->
    <!-- column -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Radar Chart</h4>
                <div id="radar-chart" style="width:100%; height:400px;"></div>
            </div>
        </div>
    </div>
    <!-- column -->
    <!-- column -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Doughnut Chart</h4>
                <div id="doughnut-chart" style="width:100%; height:400px;"></div>
            </div>
        </div>
    </div>
    <!-- column -->
    <!-- column -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Internet Speed</h4>
                <div id="gauge-chart" style="width:100%; height:400px;"></div>
            </div>
        </div>
    </div>
    <!-- column -->
    <!-- column -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Market Rates</h4>
                <div id="gauge2-chart" style="width:100%; height:400px;"></div>
            </div>
        </div>
    </div>
    <!-- column -->
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->