@push('after-scripts')

    <!-- Chart JS -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/Chart.js/chartjs.init.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/Chart.js/Chart.min.js"></script>

@endpush

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <!-- column -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Line Chart</h4>
                <div>
                    <canvas id="chart1" height="150"></canvas>
                </div>
            </div>
        </div>
    </div>
    <!-- column -->
    <!-- column -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Bar Chart</h4>
                <div>
                    <canvas id="chart2" height="150"></canvas>
                </div>
            </div>
        </div>
    </div>
    <!-- column -->
    <!-- column -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Pie Chart</h4>
                <div>
                    <canvas id="chart3" height="150"></canvas>
                </div>
            </div>
        </div>
    </div>
    <!-- column -->
    <!-- column -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Doughnut Chart</h4>
                <div>
                    <canvas id="chart4" height="150"> </canvas>
                </div>
            </div>
        </div>
    </div>
    <!-- column -->
    <!-- column -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Polar Area Chart</h4>
                <div>
                    <canvas id="chart5" height="150"></canvas>
                </div>
            </div>
        </div>
    </div>
    <!-- column -->
    <!-- column -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Radar Chart</h4>
                <div>
                    <canvas id="chart6" height="150"></canvas>
                </div>
            </div>
        </div>
    </div>
    <!-- column -->
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->