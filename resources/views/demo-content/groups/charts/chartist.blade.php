@push('before-styles')

    <!-- chartist CSS -->
    <link href="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
    <link href="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">

@endpush

@push('after-scripts')

    <!-- chartist chart -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/chartist-js/dist/chartist.min.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/chartist-js/dist/chartist-init.js"></script>

@endpush

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <!-- column -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Simple Line Chart</h4>
                <div class="ct-sm-line-chart" style="height: 400px;"></div>
            </div>
        </div>
    </div>
    <!-- column -->
    <!-- column -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Line Chart with Area</h4>
                <div class="ct-area-ln-chart" style="height: 400px;"></div>>
            </div>
        </div>
    </div>
    <!-- column -->
    <!-- column -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">BI-Polar Line Chart</h4>
                <div id="ct-polar-chart" style="height: 400px;"></div>
            </div>
        </div>
    </div>
    <!-- column -->
    <!-- column -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Animation Chart</h4>
                <div class="ct-animation-chart" style="height: 400px;"></div>
            </div>
        </div>
    </div>
    <!-- column -->
    <!-- column -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Radar Chart</h4>
                <div class="ct-bar-chart" style="height: 400px;"></div>
            </div>
        </div>
    </div>
    <!-- column -->
    <!-- column -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">SVG animation chart</h4>
                <div class="ct-svg-chart" style="height: 400px;"></div>
            </div>
        </div>
    </div>
    <!-- column -->
    <!-- column -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">GAUGE CHART</h4>
                <div class="ct-gauge-chart" style="height: 400px;"></div>
            </div>
        </div>
    </div>
    <!-- column -->
    <!-- column -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Donute chart</h4>
                <div class="ct-donute-chart" style="height: 400px;"></div>
            </div>
        </div>
    </div>
    <!-- column -->
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->