<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row el-element-overlay">
    <div class="col-md-12">
        <h4 class="card-title">Fade-in effect</h4>
        <h6 class="card-subtitle m-b-20 text-muted">You can use by default <code>el-overlay</code></h6> </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/1.jpg" alt="user" />
                    <div class="el-overlay">
                        <ul class="el-info">
                            <li><a class="btn default btn-outline image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/plugins/images/users/1.jpg"><i class="icon-magnifier"></i></a></li>
                            <li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="el-card-content">
                    <h3 class="box-title">Genelia Deshmukh</h3> <small>Managing Director</small>
                    <br/> </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/2.jpg" alt="user" />
                    <div class="el-overlay">
                        <ul class="el-info">
                            <li><a class="btn default btn-outline image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/plugins/images/users/1.jpg"><i class="icon-magnifier"></i></a></li>
                            <li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="el-card-content">
                    <h3 class="box-title">Genelia Deshmukh</h3> <small>Managing Director</small>
                    <br/> </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/3.jpg" alt="user" />
                    <div class="el-overlay">
                        <ul class="el-info">
                            <li><a class="btn default btn-outline image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/plugins/images/users/1.jpg"><i class="icon-magnifier"></i></a></li>
                            <li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="el-card-content">
                    <h3 class="box-title">Genelia Deshmukh</h3> <small>Managing Director</small>
                    <br/> </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/4.jpg" alt="user" />
                    <div class="el-overlay">
                        <ul class="el-info">
                            <li><a class="btn default btn-outline image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/plugins/images/users/1.jpg"><i class="icon-magnifier"></i></a></li>
                            <li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="el-card-content">
                    <h3 class="box-title">Genelia Deshmukh</h3> <small>Managing Director</small>
                    <br/> </div>
            </div>
        </div>
    </div>
</div>
<div class="row el-element-overlay">
    <div class="col-md-12">
        <h4 class="card-title">Scroll down effect</h4>
        <h6 class="card-subtitle m-b-20 text-muted">You can use scroll down effect <code>el-overlay scrl-dwn</code></h6> </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/5.jpg" alt="user" />
                    <div class="el-overlay scrl-dwn">
                        <ul class="el-info">
                            <li><a class="btn default btn-outline image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/plugins/images/users/1.jpg"><i class="icon-magnifier"></i></a></li>
                            <li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="el-card-content">
                    <h3 class="box-title">Genelia Deshmukh</h3> <small>Managing Director</small>
                    <br/> </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/6.jpg" alt="user" />
                    <div class="el-overlay scrl-dwn">
                        <ul class="el-info">
                            <li><a class="btn default btn-outline image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/plugins/images/users/1.jpg"><i class="icon-magnifier"></i></a></li>
                            <li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="el-card-content">
                    <h3 class="box-title">Genelia Deshmukh</h3> <small>Managing Director</small>
                    <br/> </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/7.jpg" alt="user" />
                    <div class="el-overlay scrl-dwn">
                        <ul class="el-info">
                            <li><a class="btn default btn-outline image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/plugins/images/users/1.jpg"><i class="icon-magnifier"></i></a></li>
                            <li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="el-card-content">
                    <h3 class="box-title">Genelia Deshmukh</h3> <small>Managing Director</small>
                    <br/> </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/8.jpg" alt="user" />
                    <div class="el-overlay scrl-dwn">
                        <ul class="el-info">
                            <li><a class="btn default btn-outline image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/plugins/images/users/1.jpg"><i class="icon-magnifier"></i></a></li>
                            <li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="el-card-content">
                    <h3 class="box-title">Genelia Deshmukh</h3> <small>Managing Director</small>
                    <br/> </div>
            </div>
        </div>
    </div>
</div>
<div class="row el-element-overlay">
    <div class="col-md-12">
        <h4 class="card-title">Scroll up effect</h4>
        <h6 class="card-subtitle m-b-20 text-muted">You can use by default <code>el-overlay scrl-up</code></h6> </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/1.jpg" alt="user" />
                    <div class="el-overlay scrl-up">
                        <ul class="el-info">
                            <li><a class="btn default btn-outline image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/plugins/images/users/1.jpg"><i class="icon-magnifier"></i></a></li>
                            <li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="el-card-content">
                    <h3 class="box-title">Genelia Deshmukh</h3> <small>Managing Director</small>
                    <br/> </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/2.jpg" alt="user" />
                    <div class="el-overlay scrl-up">
                        <ul class="el-info">
                            <li><a class="btn default btn-outline image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/plugins/images/users/1.jpg"><i class="icon-magnifier"></i></a></li>
                            <li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="el-card-content">
                    <h3 class="box-title">Genelia Deshmukh</h3> <small>Managing Director</small>
                    <br/> </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/3.jpg" alt="user" />
                    <div class="el-overlay scrl-up">
                        <ul class="el-info">
                            <li><a class="btn default btn-outline image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/plugins/images/users/1.jpg"><i class="icon-magnifier"></i></a></li>
                            <li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="el-card-content">
                    <h3 class="box-title">Genelia Deshmukh</h3> <small>Managing Director</small>
                    <br/> </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/4.jpg" alt="user" />
                    <div class="el-overlay scrl-up">
                        <ul class="el-info">
                            <li><a class="btn default btn-outline image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/plugins/images/users/1.jpg"><i class="icon-magnifier"></i></a></li>
                            <li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="el-card-content">
                    <h3 class="box-title">Genelia Deshmukh</h3> <small>Managing Director</small>
                    <br/> </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->