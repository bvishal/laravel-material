@push('before-styles')

    <!--Range slider CSS -->
    <link href="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet">
    <link href="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/ion-rangeslider/css/ion.rangeSlider.skinModern.css" rel="stylesheet">

@endpush

@push('after-scripts')

    <!-- Range slider  -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider-init.js"></script>

@endpush

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <!-- .row -->
                <div class="row">
                    <!-- .col-lg-12 -->
                    <div class="col-md-12">
                        <h4 class="card-title">Start without params</h4>
                        <div id="range_01"></div>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-md-12 m-t-40 m-b-40">
                        <hr>
                    </div>
                    <!-- .col-lg-12 -->
                    <div class="col-md-12">
                        <h4 class="card-title">Set min value, max value and start point</h4>
                        <div id="range_02"></div>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-md-12 m-t-40 m-b-40">
                        <hr>
                    </div>
                    <!-- .col-lg-12 -->
                    <div class="col-md-12">
                        <h4 class="card-title">Set type to double and specify range, also showing grid and adding prefix "$"</h4>
                        <div id="range_03"></div>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-md-12 m-t-40 m-b-40">
                        <hr>
                    </div>
                    <!-- .col-lg-12 -->
                    <div class="col-md-12">
                        <h4 class="card-title">Set up range with negative values</h4>
                        <div id="range_04"></div>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-md-12 m-t-40 m-b-40">
                        <hr>
                    </div>
                    <!-- .col-lg-12 -->
                    <div class="col-md-12">
                        <h4 class="card-title">Whant to show that max number is not the biggest one?</h4>
                        <div id="range_16"></div>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-md-12 m-t-40 m-b-40">
                        <hr>
                    </div>
                    <!-- .col-lg-12 -->
                    <div class="col-md-12">
                        <h4 class="card-title">Remove double decoration</h4>
                        <div id="range_18"></div>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-md-12 m-t-40 m-b-40">
                        <hr>
                    </div>
                    <!-- .col-lg-12 -->
                    <div class="col-md-12">
                        <h4 class="card-title">Visual details</h4>
                        <div id="range_22"></div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->