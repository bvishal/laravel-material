@push('after-scripts')

    <!-- Session-timeout -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/session-timeout/jquery.sessionTimeout.min.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/session-timeout/session-timeout-init.js"></script>

@endpush

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Session Timeout Notification Control</h4>
                <p>After a set amount of time, a dialog is shown to the user with the option to either log out now, or stay connected. If log out now is selected, the page is redirected to a logout URL. If stay connected is selected, a keep-alive URL is requested through AJAX. If no options is selected after another set amount of time, the page is automatically redirected to a timeout URL.</p>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->