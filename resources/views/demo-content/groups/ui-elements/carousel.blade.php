<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<!-- row -->
<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active"> <img class="img-responsive" src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img1.jpg" alt="First slide"> </div>
                        <div class="carousel-item"> <img class="img-responsive" src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img2.jpg" alt="Second slide"> </div>
                        <div class="carousel-item"> <img class="img-responsive" src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img3.jpg" alt="Third slide"> </div>
                    </div>
                </div>
                <div class="highlight"> <pre><code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">id=</span><span class="s">"carouselExampleSlidesOnly"</span> <span class="na">class=</span><span class="s">"carousel slide"</span> <span class="na">data-ride=</span><span class="s">"carousel"</span><span class="nt">&gt;</span>
      <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"carousel-inner"</span> <span class="na">role=</span><span class="s">"listbox"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"carousel-item active"</span><span class="nt">&gt;</span>
          <span class="nt">&lt;img</span> <span class="na">class=</span><span class="s">"d-block img-fluid"</span> <span class="na">src=</span><span class="s">"..."</span> <span class="na">alt=</span><span class="s">"First slide"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"carousel-item"</span><span class="nt">&gt;</span>
          <span class="nt">&lt;img</span> <span class="na">class=</span><span class="s">"d-block img-fluid"</span> <span class="na">src=</span><span class="s">"..."</span> <span class="na">alt=</span><span class="s">"Second slide"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"carousel-item"</span><span class="nt">&gt;</span>
          <span class="nt">&lt;img</span> <span class="na">class=</span><span class="s">"d-block img-fluid"</span> <span class="na">src=</span><span class="s">"..."</span> <span class="na">alt=</span><span class="s">"Third slide"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;/div&gt;</span>
      <span class="nt">&lt;/div&gt;</span>
    <span class="nt">&lt;/div&gt;</span></code></pre> </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active"> <img class="img-responsive" src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img4.jpg" alt="First slide"> </div>
                        <div class="carousel-item"> <img class="img-responsive" src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img5.jpg" alt="Second slide"> </div>
                        <div class="carousel-item"> <img class="img-responsive" src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img6.jpg" alt="Third slide"> </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
                </div>
                <div class="highlight"> <pre><code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"carousel-inner"</span> <span class="na">role=</span><span class="s">"listbox"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"carousel-item active"</span><span class="nt">&gt;</span>
          <span class="nt">&lt;img</span> <span class="na">class=</span><span class="s">"d-block img-fluid"</span> <span class="na">src=</span><span class="s">"..."</span> <span class="na">alt=</span><span class="s">"First slide"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"carousel-item"</span><span class="nt">&gt;</span>
          <span class="nt">&lt;img</span> <span class="na">class=</span><span class="s">"d-block img-fluid"</span> <span class="na">src=</span><span class="s">"..."</span> <span class="na">alt=</span><span class="s">"Second slide"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"carousel-item"</span><span class="nt">&gt;</span>
          <span class="nt">&lt;img</span> <span class="na">class=</span><span class="s">"d-block img-fluid"</span> <span class="na">src=</span><span class="s">"..."</span> <span class="na">alt=</span><span class="s">"Third slide"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;/div&gt;</span>
      <span class="nt">&lt;/div&gt;</span>
      <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"carousel-control-prev"</span> <span class="na">href=</span><span class="s">"#carouselExampleIndicators"</span> <span class="na">role=</span><span class="s">"button"</span> <span class="na">data-slide=</span><span class="s">"prev"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"carousel-control-prev-icon"</span> <span class="na">aria-hidden=</span><span class="s">"true"</span><span class="nt">&gt;&lt;/span&gt;</span>
        <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"sr-only"</span><span class="nt">&gt;</span>Previous<span class="nt">&lt;/span&gt;</span>
      <span class="nt">&lt;/a&gt;</span>
      <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"carousel-control-next"</span> <span class="na">href=</span><span class="s">"#carouselExampleIndicators"</span> <span class="na">role=</span><span class="s">"button"</span> <span class="na">data-slide=</span><span class="s">"next"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"carousel-control-next-icon"</span> <span class="na">aria-hidden=</span><span class="s">"true"</span><span class="nt">&gt;&lt;/span&gt;</span>
        <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"sr-only"</span><span class="nt">&gt;</span>Next<span class="nt">&lt;/span&gt;</span>
      <span class="nt">&lt;/a&gt;</span>
    <span class="nt">&lt;/div&gt;</span></code></pre> </div>
            </div>
        </div>
    </div>
</div>
<!-- End row -->
<!-- row -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <h4 class="card-title">With indicators</h4>
                        <h6 class="card-subtitle">You can also add the indicators to the carousel, alongside the controls, too.</h6>
                        <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
                                <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner" role="listbox">
                                <div class="carousel-item active">
                                    <img class="img-responsive" src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img3.jpg" alt="First slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="img-responsive" src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img4.jpg" alt="Second slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="img-responsive" src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img5.jpg" alt="Third slide">
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <h4 class="card-title">With captions</h4>
                        <h6 class="card-subtitle">Add captions to your slides easily with the <code>.carousel-caption</code></h6>
                        <div id="carouselExampleIndicators3" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleIndicators3" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselExampleIndicators3" data-slide-to="1"></li>
                                <li data-target="#carouselExampleIndicators3" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner" role="listbox">
                                <div class="carousel-item active">
                                    <img class="img-responsive" src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img6.jpg" alt="First slide">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h3 class="text-white">First title goes here</h3>
                                        <p>this is the subcontent you can use this</p>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img class="img-responsive" src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img3.jpg" alt="Second slide">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h3 class="text-white">Second title goes here</h3>
                                        <p>this is the subcontent you can use this</p>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img class="img-responsive" src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img4.jpg" alt="Third slide">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h3 class="text-white">Third title goes here</h3>
                                        <p>this is the subcontent you can use this</p>
                                    </div>
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators3" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators3" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End row -->
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->