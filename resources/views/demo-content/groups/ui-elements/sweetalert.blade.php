@push('before-styles')

    <!--alerts CSS -->
    <link href="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">

@endpush

@push('after-scripts')

    <!-- Sweet-Alert  -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/sweetalert/jquery.sweet-alert.custom.js"></script>

@endpush

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-md-3">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">A basic message <small>(Click on image)</small> </h4>
                <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/alert/alert.png" alt="alert" class="img-responsive model_img" id="sa-basic">
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Title with a text under <small>(Click on image)</small></h4>
                <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/alert/alert2.png" alt="alert" class="img-responsive model_img" id="sa-title">
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Success Message <small>(Click on image)</small></h4>
                <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/alert/alert3.png" alt="alert" class="img-responsive model_img" id="sa-success">
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Warning message <small>(Click on image)</small></h4>
                <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/alert/alert4.png" alt="alert" class="img-responsive model_img" id="sa-warning">
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">A basic message <small>(Click on image)</small> </h4>
                <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/alert/alert5.png" alt="alert" class="img-responsive model_img" id="sa-params">
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Alert with Image <small>(Click on image)</small></h4>
                <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/alert/alert7.png" alt="alert" class="img-responsive model_img" id="sa-image">
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Alert with time <small>(Click on image)</small></h4>
                <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/alert/alert6.png" alt="alert" class="img-responsive model_img" id="sa-close">
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->