<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Default bootstrap breadcrumb</h4>
                <h6 class="card-subtitle">use class <code>.breadcrumb to ol</code></h6>
                <ol class="breadcrumb m-b-10">
                    <li class="breadcrumb-item active">Home</li>
                </ol>
                <ol class="breadcrumb m-b-10">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Library</li>
                </ol>
                <ol class="breadcrumb m-b-10">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Library</a></li>
                    <li class="breadcrumb-item active">Data</li>
                </ol>
                <br>
                <br>
                <h4 class="card-title">Nav breadcrumb</h4>
                <h6 class="card-subtitle">use class <code>.breadcrumb to nav</code> similar to navigation</h6>
                <nav class="breadcrumb">
                    <a class="breadcrumb-item" href="#">Home</a>
                    <a class="breadcrumb-item" href="#">Library</a>
                    <a class="breadcrumb-item" href="#">Data</a>
                    <span class="breadcrumb-item active">Bootstrap</span>
                </nav>
                <br>
                <br>
                <h4 class="card-title">Colored breadcrumb</h4>
                <h6 class="card-subtitle">use class <code>.bc-colored .bg-primary</code> similar to navigation</h6>
                <ol class="breadcrumb bc-colored m-b-30 bg-primary">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Library</li>
                </ol>
                <ol class="breadcrumb bc-colored m-b-30 bg-info">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Library</li>
                </ol>
                <ol class="breadcrumb bc-colored m-b-30 bg-warning">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Library</li>
                </ol>
                <ol class="breadcrumb bc-colored m-b-30 bg-success">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Library</li>
                </ol>
                <ol class="breadcrumb bc-colored m-b-30 bg-danger">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Library</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->