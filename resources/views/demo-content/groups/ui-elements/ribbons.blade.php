<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-12">
        <h4 class="card-title">Ribbons default</h4>
        <h6 class="card-subtitle m-b-20">Add class: <code>.ribbon</code>. It must be inside <code>.ribbon-wrapper</code> class. Content should be inside <code>.ribbon-content</code> class.</h6>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-xlg-2 col-xs-12">
                <div class="ribbon-wrapper card">
                    <div class="ribbon ribbon-default">Ribbon</div>
                    <p class="ribbon-content">Duis mollis, est non commodo luctus, nisi erat porttitor ligula</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-xlg-2 col-xs-12">
                <div class="ribbon-wrapper card">
                    <div class="ribbon ribbon-danger">Ribbon</div>
                    <p class="ribbon-content">Duis mollis, est non commodo luctus, nisi erat porttitor ligula</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-xlg-2 col-xs-12">
                <div class="ribbon-wrapper card">
                    <div class="ribbon ribbon-success">Ribbon</div>
                    <p class="ribbon-content">Duis mollis, est non commodo luctus, nisi erat porttitor ligula</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-xlg-2 col-xs-12">
                <div class="ribbon-wrapper card">
                    <div class="ribbon ribbon-primary">Ribbon</div>
                    <p class="ribbon-content">Duis mollis, est non commodo luctus, nisi erat porttitor ligula</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-xlg-2 col-xs-12">
                <div class="ribbon-wrapper card">
                    <div class="ribbon ribbon-info">Ribbon</div>
                    <p class="ribbon-content">Duis mollis, est non commodo luctus, nisi erat porttitor ligula</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-xlg-2 col-xs-12">
                <div class="ribbon-wrapper card">
                    <div class="ribbon ribbon-warning">Ribbon</div>
                    <p class="ribbon-content">Duis mollis, est non commodo luctus, nisi erat porttitor ligula</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 m-t-40">
        <h4 class="card-title">Ribbons right align</h4>
        <h6 class="card-subtitle m-b-20">Add class: <code>.ribbon-right</code></h6>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-xlg-2 col-xs-12">
                <div class="ribbon-wrapper card">
                    <div class="ribbon ribbon-default ribbon-right">Ribbon</div>
                    <p class="ribbon-content">Duis mollis, est non commodo luctus, nisi erat porttitor ligula</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-xlg-2 col-xs-12">
                <div class="ribbon-wrapper card">
                    <div class="ribbon ribbon-danger ribbon-right">Ribbon</div>
                    <p class="ribbon-content">Duis mollis, est non commodo luctus, nisi erat porttitor ligula</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-xlg-2 col-xs-12">
                <div class="ribbon-wrapper card">
                    <div class="ribbon ribbon-success ribbon-right">Ribbon</div>
                    <p class="ribbon-content">Duis mollis, est non commodo luctus, nisi erat porttitor ligula</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-xlg-2 col-xs-12">
                <div class="ribbon-wrapper card">
                    <div class="ribbon ribbon-primary ribbon-right">Ribbon</div>
                    <p class="ribbon-content">Duis mollis, est non commodo luctus, nisi erat porttitor ligula</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-xlg-2 col-xs-12">
                <div class="ribbon-wrapper card">
                    <div class="ribbon ribbon-info ribbon-right">Ribbon</div>
                    <p class="ribbon-content">Duis mollis, est non commodo luctus, nisi erat porttitor ligula</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-xlg-2 col-xs-12">
                <div class="ribbon-wrapper card">
                    <div class="ribbon ribbon-warning ribbon-right">Ribbon</div>
                    <p class="ribbon-content">Duis mollis, est non commodo luctus, nisi erat porttitor ligula</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 m-t-40">
        <h4 class="card-title">Ribbons vertical</h4>
        <h6 class="card-subtitle m-b-20">Add class: <code>.ribbon-vertical-l</code> to display on left. Add class: <code>.ribbon-vertical-r</code>to display on right</h6>
        <div class="row">
            <div class="col-lg-2 col-md-4 col-sm-3 col-xs-12">
                <div class="ribbon-vwrapper card">
                    <div class="ribbon ribbon-default ribbon-vertical-l"><i class="fa fa-check-circle"></i></div>
                    <p class="ribbon-content">Duis mollis, est non commodo luctus, nisi erat porttitor ligula</p>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-3 col-xs-12">
                <div class="ribbon-vwrapper card">
                    <div class="ribbon ribbon-danger ribbon-vertical-l"><i class="fa fa-check-circle"></i></div>
                    <p class="ribbon-content">Duis mollis, est non commodo luctus, nisi erat porttitor ligula</p>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-3 col-xs-12">
                <div class="ribbon-vwrapper card">
                    <div class="ribbon ribbon-success ribbon-vertical-l"><i class="fa fa-check-circle"></i></div>
                    <p class="ribbon-content">Duis mollis, est non commodo luctus, nisi erat porttitor ligula</p>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-3 col-xs-12">
                <div class="ribbon-vwrapper-reverse card">
                    <div class="ribbon ribbon-primary  ribbon-vertical-r"><i class="fa fa-check-circle"></i></div>
                    <p class="ribbon-content">Duis mollis, est non commodo luctus, nisi erat porttitor ligula</p>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-3 col-xs-12">
                <div class="ribbon-vwrapper-reverse card">
                    <div class="ribbon ribbon-info ribbon-vertical-r"><i class="fa fa-check-circle"></i></div>
                    <p class="ribbon-content">Duis mollis, est non commodo luctus, nisi erat porttitor ligula</p>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-3 col-xs-12">
                <div class="ribbon-vwrapper-reverse card">
                    <div class="ribbon ribbon-warning ribbon-vertical-r"><i class="fa fa-check-circle"></i></div>
                    <p class="ribbon-content">Duis mollis, est non commodo luctus, nisi erat porttitor ligula</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 m-t-40">
        <h4 class="card-title">Bookmarked ribbons</h4>
        <h6 class="card-subtitle m-b-20">Add class: <code>.ribbon-bookmark</code> right after the <code>.ribbon</code> class.  </h6>
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-3 col-xs-12">
                <div class="ribbon-wrapper card">
                    <div class="ribbon ribbon-bookmark  ribbon-default">Ribbon</div>
                    <p class="ribbon-content">Duis mollis, est non commodo luctus, nisi erat porttitor ligula</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-3 col-xs-12">
                <div class="ribbon-wrapper-reverse card">
                    <div class="ribbon ribbon-bookmark ribbon-right ribbon-danger">Ribbon</div>
                    <p class="ribbon-content">Duis mollis, est non commodo luctus, nisi erat porttitor ligula</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-3 col-xs-12">
                <div class="ribbon-vwrapper p-b-40 p-t-30 card">
                    <div class="ribbon ribbon-bookmark ribbon-vertical-l ribbon-info"><i class="fa fa-heart"></i></div>
                    <p class="ribbon-content">Duis mollis, est non commodo luctus, nisi erat porttitor ligula</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-3 col-xs-12">
                <div class="ribbon-wrapper-reverse card">
                    <div class="ribbon ribbon-bookmark ribbon-vertical-r ribbon-success"><i class="fa fa-heart"></i></div>
                    <p class="ribbon-content">Duis mollis, est non commodo luctus, nisi erat porttitor ligula</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 m-t-40">
        <h4 class="card-title">Corner ribbons</h4>
        <h6 class="card-subtitle m-b-20">Add class: <code>.ribbon-corner</code>. </h6>
        <div class="row m-b-10">
            <div class="col-lg-3 col-md-4 col-sm-3 col-xs-12">
                <div class="ribbon-wrapper card">
                    <div class="ribbon ribbon-corner ribbon-info"><i class="fa fa-heart"></i></div>
                    <p class="ribbon-content">Duis mollis, est non commodo luctus, nisi erat porttitor ligula</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-3 col-xs-12">
                <div class="ribbon-wrapper-reverse card">
                    <div class="ribbon ribbon-corner ribbon-right ribbon-info"><i class="fa fa-heart"></i></div>
                    <p class="ribbon-content">Duis mollis, est non commodo luctus, nisi erat porttitor ligula</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-3 col-xs-12">
                <div class="ribbon-wrapper-bottom card">
                    <div class="ribbon ribbon-corner ribbon-info ribbon-bottom"><i class="fa fa-heart"></i></div>
                    <p class="ribbon-content">Duis mollis, est non commodo luctus, nisi erat porttitor ligula</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-3 col-xs-12">
                <div class="ribbon-wrapper-right-bottom card">
                    <div class="ribbon ribbon-corner ribbon-info ribbon-right ribbon-bottom"><i class="fa fa-heart"></i></div>
                    <p class="ribbon-content">Duis mollis, est non commodo luctus, nisi erat porttitor ligula</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->