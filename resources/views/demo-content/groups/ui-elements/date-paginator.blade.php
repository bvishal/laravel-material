@push('before-styles')
    <link href="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet">
@endpush

@push('after-scripts')

    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/date-paginator/moment.min.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/date-paginator/bootstrap-datepaginator.min.js"></script>
    <script type="text/javascript">
        var datepaginator = function() {
            return {
                init: function() {
                    $("#paginator1").datepaginator(),
                        $("#paginator2").datepaginator({
                            size: "large"
                        }),
                        $("#paginator3").datepaginator({
                            size: "small"
                        }),
                        $("#paginator4").datepaginator({
                            onSelectedDateChanged: function(a, t) {
                                alert("Selected date: " + moment(t).format("Do, MMM YYYY"))
                            }
                        })
                }
            }
        }();
        jQuery(document).ready(function() {
            datepaginator.init()
        });
    </script>

@endpush


<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Date Paginator</h4>
                <h6 class="card-subtitle">A jQuery plugin which takes Twitter Bootstrap's already great pagination component and injects a bit of date based magic. In the process creating a hugely simplified and modularised way of paging date based results in your application.</h6>
                <h4 class="card-title m-t-40">Default:</h4>
                <div id="paginator1"></div>
                <h4 class="card-title m-t-40">Large:</h4>
                <div id="paginator2"></div>
                <h4 class="card-title m-t-40">Small:</h4>
                <div id="paginator3"></div>
                <h4 class="card-title m-t-40">Onselect:</h4>
                <div id="paginator4"></div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->