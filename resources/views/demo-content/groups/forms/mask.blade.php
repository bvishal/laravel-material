<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Form Addons</h4>
                <h6 class="card-subtitle">Use the button classes on an <code>data-mask</code> to the input element.</h6>
                <form action="#">
                    <div class="form-group">
                        <label>Phone</label>
                        <input type="text" placeholder="" data-mask="(999) 999-9999" class="form-control">
                        <span class="font-13 text-muted">(999) 999-9999</span> </div>
                    <div class="form-group">
                        <label>Date</label>
                        <input type="text" placeholder="" data-mask="99/99/9999" class="form-control">
                        <span class="font-13 text-muted">dd/mm/yyyy</span> </div>
                    <div class="form-group">
                        <label>SSN field 1</label>
                        <input type="text" placeholder="" data-mask="999-99-9999" class="form-control">
                        <span class="font-13 text-muted">e.g "999-99-9999"</span> </div>
                    <div class="form-group">
                        <label>Phone field + ext.</label>
                        <input type="text" placeholder="" data-mask="+40 999 999 999" class="form-control">
                        <span class="font-13 text-muted">+40 999 999 999</span> </div>
                    <div class="form-group">
                        <label>Product Key</label>
                        <input type="text" placeholder="" data-mask="a*-999-a999" class="form-control">
                        <span class="font-13 text-muted">e.g a*-999-a999</span> </div>
                </form>
                <form action="#">
                    <div class="form-group">
                        <label>Currency</label>
                        <input type="text" placeholder="" data-mask="$ 999,999,999.99" class="form-control">
                        <span class="font-13 text-muted">$ 999,999,999.99</span> </div>
                    <div class="form-group">
                        <label>Date 2</label>
                        <input type="text" placeholder="" data-mask="99-99-9999" class="form-control">
                        <span class="font-13 text-muted">dd-mm-yyyy</span> </div>
                    <div class="form-group">
                        <label>Eye Script</label>
                        <input type="text" placeholder="" data-mask="~9.99 ~9.99 999" class="form-control">
                        <span class="font-13 text-muted">~9.99 ~9.99 999</span> </div>
                    <div class="form-group">
                        <label>Percent</label>
                        <input type="text" placeholder="" data-mask="99%" class="form-control">
                        <span class="font-13 text-muted">e.g "99%"</span> </div>
                    <div class="form-group m-b-0">
                        <label>Pc Ip</label>
                        <input type="text" placeholder="" data-mask="999.999.999.9999" class="form-control">
                        <span class="font-13 text-muted">e.g "999.999.999.9999"</span> </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->