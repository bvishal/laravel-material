@push('before-styles')

    <link rel="stylesheet" href="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/html5-editor/bootstrap-wysihtml5.css" />

@endpush

@push('after-scripts')

    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/tinymce/tinymce.min.js"></script>
    <script>
        $(document).ready(function() {

            if ($("#mymce").length > 0) {
                tinymce.init({
                    selector: "textarea#mymce",
                    theme: "modern",
                    height: 300,
                    plugins: [
                        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                        "save table contextmenu directionality emoticons template paste textcolor"
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",

                });
            }
        });
    </script>

@endpush

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Tinymce wysihtml5</h4>
                <h6 class="card-subtitle">Bootstrap html5 editor</h6>
                <form method="post">
                    <textarea id="mymce" name="area"></textarea>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->