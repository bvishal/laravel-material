@push('before-styles')

    <link rel="stylesheet" href="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/dropify/dist/css/dropify.min.css">

@endpush

@push('after-scripts')

    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/dropify/dist/js/dropify.min.js"></script>
    <script>
        $(document).ready(function() {
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove: 'Supprimer',
                    error: 'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element) {
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element) {
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element) {
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e) {
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });
    </script>

@endpush

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-lg-6 col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">File Upload1</h4>
                <label for="input-file-now">Your so fresh input file — Default version</label>
                <input type="file" id="input-file-now" class="dropify" />
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">File Upload2</h4>
                <label for="input-file-now-custom-1">You can add a default value</label>
                <input type="file" id="input-file-now-custom-1" class="dropify" data-default-file="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/dropify/src/images/test-image-1.jpg" />
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6 col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">File Upload3</h4>
                <label for="input-file-now-custom-2">You can set the height</label>
                <input type="file" id="input-file-now-custom-2" class="dropify" data-height="500" />
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">File Upload4</h4>
                <label for="input-file-now-custom-3">You can combine options</label>
                <input type="file" id="input-file-now-custom-3" class="dropify" data-height="500" data-default-file="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/dropify/src/images/test-image-2.jpg" />
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6 col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">File Upload5</h4>
                <label for="input-file-max-fs">You can add a max file size</label>
                <input type="file" id="input-file-max-fs" class="dropify" data-max-file-size="2M" />
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">File Upload6</h4>
                <label for="input-file-disable-remove">You can disable remove button</label>
                <input type="file" id="input-file-disable-remove" class="dropify" data-show-remove="false" />
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->