@push('before-styles')

    <!-- summernotes CSS -->
    <link href="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/summernote/dist/summernote.css" rel="stylesheet" />

@endpush

@push('after-scripts')

    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/summernote/dist/summernote.min.js"></script>
    <script>
        jQuery(document).ready(function() {

            $('.summernote').summernote({
                height: 350, // set editor height
                minHeight: null, // set minimum height of editor
                maxHeight: null, // set maximum height of editor
                focus: false // set focus to editable area after initializing summernote
            });

            $('.inline-editor').summernote({
                airMode: true
            });

        });

        window.edit = function() {
            $(".click2edit").summernote()
        },
            window.save = function() {
                $(".click2edit").summernote('destroy');
            }
    </script>

@endpush

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="summernote">
                    <h3>Default Summernote</h3>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="inline-editor">
                    <h4 class="card-title m-b-40">You can select content and edit inline</h4>
                    <h3>Title Heading will be <b>apear here</b></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elitconsectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud Exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    <ul class="list-icons">
                        <li><i class="fa fa-check text-success"></i> Lorem ipsum dolor sit amet</li>
                        <li><i class="fa fa-check text-success"></i> Consectetur adipiscing elit</li>
                        <li><i class="fa fa-check text-success"></i> Integer molestie lorem at massa </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="click2edit m-b-40">Click on Edite button and change the text then save it.</div>
                <button id="edit" class="btn btn-info btn-rounded" onclick="edit()" type="button">Edit</button>
                <button id="save" class="btn btn-success btn-rounded" onclick="save()" type="button">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->