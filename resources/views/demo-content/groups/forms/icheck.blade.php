@push('after-scripts')

    <!-- icheck -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/icheck/icheck.min.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/icheck/icheck.init.js"></script>

@endpush

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="card-title">Icheck with minimal skin</h4>
                        <h6 class="card-subtitle"></h6>
                        <div class="skin skin-minimal">
                            <form>
                                <div class="form-group">
                                    <label>Icheck different Colors</label>
                                    <div class="input-group">
                                        <ul class="icolors">
                                            <li></li>
                                            <li class="red active"></li>
                                            <li class="green"></li>
                                            <li class="blue"></li>
                                            <li class="orange"></li>
                                            <li class="yellow"></li>
                                            <li class="purple"></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Checkbox &amp; Radio List</label>
                                    <div class="input-group">
                                        <ul class="icheck-list">
                                            <li>
                                                <input type="checkbox" class="check" id="minimal-checkbox-1">
                                                <label for="minimal-checkbox-1">Checkbox 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" class="check" id="minimal-checkbox-2" checked>
                                                <label for="minimal-checkbox-2">Checkbox 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" class="check" id="minimal-checkbox-disabled" disabled>
                                                <label for="minimal-checkbox-disabled">Disabled</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" class="check" id="minimal-checkbox-disabled-checked" checked disabled>
                                                <label for="minimal-checkbox-disabled-checked">Disabled &amp; checked</label>
                                            </li>
                                        </ul>
                                        <ul class="icheck-list">
                                            <li>
                                                <input tabindex="7" type="radio" class="check" id="minimal-radio-1" name="minimal-radio">
                                                <label for="minimal-radio-1">Radio button 1</label>
                                            </li>
                                            <li>
                                                <input tabindex="8" type="radio" class="check" id="minimal-radio-2" name="minimal-radio" checked>
                                                <label for="minimal-radio-2">Radio button 2</label>
                                            </li>
                                            <li>
                                                <input type="radio" class="check" id="minimal-radio-disabled" disabled>
                                                <label for="minimal-radio-disabled">Disabled</label>
                                            </li>
                                            <li>
                                                <input type="radio" class="check" id="minimal-radio-disabled-checked" checked disabled>
                                                <label for="minimal-radio-disabled-checked">Disabled &amp; checked</label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- column -->
                    <div class="col-md-6">
                        <div class="skin skin-flat m-t-30">
                            <form>
                                <div class="form-group">
                                    <label>Colors</label>
                                    <div class="input-group">
                                        <ul class="icolors">
                                            <li></li>
                                            <li class="red active"></li>
                                            <li class="green"></li>
                                            <li class="blue"></li>
                                            <li class="orange"></li>
                                            <li class="yellow"></li>
                                            <li class="purple"></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Checkbox &amp; Radio List</label>
                                    <div class="input-group">
                                        <ul class="icheck-list">
                                            <li>
                                                <input type="checkbox" class="check" id="flat-checkbox-1" data-checkbox="icheckbox_flat-red">
                                                <label for="flat-checkbox-1">Checkbox 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" class="check" id="flat-checkbox-2" checked data-checkbox="icheckbox_flat-red">
                                                <label for="flat-checkbox-2">Checkbox 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" class="check" id="flat-checkbox-disabled" disabled data-checkbox="icheckbox_flat-red">
                                                <label for="flat-checkbox-disabled">Disabled</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" class="check" id="flat-checkbox-disabled-checked" checked disabled data-checkbox="icheckbox_flat-red">
                                                <label for="flat-checkbox-disabled-checked">Disabled &amp; checked</label>
                                            </li>
                                        </ul>
                                        <ul class="icheck-list">
                                            <li>
                                                <input type="radio" class="check" id="flat-radio-1" name="flat-radio" data-radio="iradio_flat-red">
                                                <label for="flat-radio-1">Radio button 1</label>
                                            </li>
                                            <li>
                                                <input type="radio" class="check" id="flat-radio-2" name="flat-radio" checked data-radio="iradio_flat-red">
                                                <label for="flat-radio-2">Radio button 2</label>
                                            </li>
                                            <li>
                                                <input type="radio" class="check" id="flat-radio-disabled" disabled data-radio="iradio_flat-red">
                                                <label for="flat-radio-disabled">Disabled</label>
                                            </li>
                                            <li>
                                                <input type="radio" class="check" id="flat-radio-disabled-checked" checked disabled data-radio="iradio_flat-red">
                                                <label for="flat-radio-disabled-checked">Disabled &amp; checked</label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- column -->
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="card-title">Icheck with minimal skin</h4>
                        <h6 class="card-subtitle"></h6>
                        <div class="skin skin-square">
                            <form>
                                <div class="form-group">
                                    <label>Colors</label>
                                    <div class="input-group">
                                        <ul class="icolors">
                                            <li></li>
                                            <li class="red active"></li>
                                            <li class="green"></li>
                                            <li class="blue"></li>
                                            <li class="orange"></li>
                                            <li class="yellow"></li>
                                            <li class="purple"></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Checkbox &amp; Radio List</label>
                                    <div class="input-group">
                                        <ul class="icheck-list">
                                            <li>
                                                <input type="checkbox" class="check" id="square-checkbox-1" data-checkbox="icheckbox_square-red">
                                                <label for="square-checkbox-1">Checkbox 1</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" class="check" id="square-checkbox-2" checked data-checkbox="icheckbox_square-red">
                                                <label for="square-checkbox-2">Checkbox 2</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" class="check" id="square-checkbox-disabled" disabled data-checkbox="icheckbox_square-red">
                                                <label for="square-checkbox-disabled">Disabled</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" class="check" id="square-checkbox-disabled-checked" checked disabled data-checkbox="icheckbox_square-red">
                                                <label for="square-checkbox-disabled-checked">Disabled &amp; checked</label>
                                            </li>
                                        </ul>
                                        <ul class="icheck-list">
                                            <li>
                                                <input type="radio" class="check" id="square-radio-1" name="square-radio" data-radio="iradio_square-red">
                                                <label for="square-radio-1">Radio button 1</label>
                                            </li>
                                            <li>
                                                <input type="radio" class="check" id="square-radio-2" name="square-radio" checked data-radio="iradio_square-red">
                                                <label for="square-radio-2">Radio button 2</label>
                                            </li>
                                            <li>
                                                <input type="radio" class="check" id="square-radio-disabled" disabled data-radio="iradio_square-red">
                                                <label for="square-radio-disabled">Disabled</label>
                                            </li>
                                            <li>
                                                <input type="radio" class="check" id="square-radio-disabled-checked" checked disabled data-radio="iradio_square-red">
                                                <label for="square-radio-disabled-checked">Disabled &amp; checked</label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- column -->
                    <div class="col-md-6">
                        <div class="skin skin-line m-t-30">
                            <form>
                                <div class="form-group">
                                    <label>Colors</label>
                                    <div class="input-group">
                                        <ul class="icolors">
                                            <li></li>
                                            <li class="red active"></li>
                                            <li class="green"></li>
                                            <li class="blue"></li>
                                            <li class="orange"></li>
                                            <li class="yellow"></li>
                                            <li class="purple"></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Checkbox &amp; Radio List</label>
                                    <div class="input-group">
                                        <ul class="icheck-list">
                                            <li>
                                                <input type="checkbox" class="check" data-checkbox="icheckbox_line-red" data-label="Checkbox 1"> </li>
                                            <li>
                                                <input type="checkbox" class="check" checked data-checkbox="icheckbox_line-red" data-label="Checkbox 2"> </li>
                                            <li>
                                                <input type="checkbox" class="check" disabled data-checkbox="icheckbox_line-red" data-label="Disabled"> </li>
                                            <li>
                                                <input type="checkbox" class="check" checked disabled data-checkbox="icheckbox_line-red" data-label="Disabled &amp; checked"> </li>
                                        </ul>
                                        <ul class="icheck-list">
                                            <li>
                                                <input type="radio" class="check" name="line-radio" data-radio="iradio_line-red" data-label="Radio button 1"> </li>
                                            <li>
                                                <input type="radio" class="check" name="line-radio" checked data-radio="iradio_line-red" data-label="Radio button 2"> </li>
                                            <li>
                                                <input type="radio" class="check" disabled data-radio="iradio_line-red" data-label="Disabled"> </li>
                                            <li>
                                                <input type="radio" class="check" checked disabled data-radio="iradio_line-red" data-label="Disabled &amp; checked"> </li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- column -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->