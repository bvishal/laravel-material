@push('before-styles')

    <link rel="stylesheet" href="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/html5-editor/bootstrap-wysihtml5.css" />

@endpush

@push('after-scripts')

    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/html5-editor/wysihtml5-0.3.0.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/html5-editor/bootstrap-wysihtml5.js"></script>
    <script>
        $(document).ready(function() {

            $('.textarea_editor').wysihtml5();


        });
    </script>

@endpush

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Bootstrap wysihtml5</h4>
                <h6 class="card-subtitle">Bootstrap html5 editor</h6>
                <form method="post">
                    <div class="form-group">
                        <textarea class="textarea_editor form-control" rows="15" placeholder="Enter text ..."></textarea>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->