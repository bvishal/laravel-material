@push('before-styles')

    <!-- Dropzone css -->
    <link href="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />

@endpush

@push('after-scripts')

    <!-- Dropzone Plugin JavaScript -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/dropzone-master/dist/dropzone.js"></script>

@endpush

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Dropzone</h4>
                <h6 class="card-subtitle">For multiple file upload put class <code>.dropzone</code> to form.</h6>
                <form action="#" class="dropzone">
                    <div class="fallback">
                        <input name="file" type="file" multiple />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->