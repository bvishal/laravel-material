@push('before-styles')

    <!-- Typehead CSS -->
    <link href="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/typeahead.js-master/dist/typehead-min.css" rel="stylesheet">

@endpush

@push('after-scripts')

    <!-- Typehead Plugin JavaScript -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/typeahead.js-master/dist/typeahead.bundle.min.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/typeahead.js-master/dist/typeahead-init.js"></script>

@endpush

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Basic usage</h4>
                <h6 class="card-subtitle">When initializing a typeahead, you pass the plugin method one or more datasets. The source of a dataset is responsible for computing a set of suggestions for a given query.</h6>
                <div id="the-basics">
                    <input class="typeahead form-control" type="text" placeholder="Countries">
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Bloodhound</h4>
                <h6 class="card-subtitle">Suggestion Engine - For more advanced use cases, rather than implementing the source for your dataset yourself, you can take advantage of Bloodhound, the typeahead.js suggestion engine.</h6>
                <div id="bloodhound">
                    <input class="typeahead form-control" type="text" placeholder="Countries">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- row -->
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Prefetch</h4>
                <h6 class="card-subtitle">Prefetched data is fetched and processed on initialization. If the browser supports local storage, the processed data will be cached there to prevent additional network requests on subsequent page loads.</h6>
                <div id="prefetch">
                    <input class="typeahead form-control" type="text" placeholder="Countries">
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Default Suggestions</h4>
                <h6 class="card-subtitle">Default suggestions can be shown for empty queries by setting the minLength option to 0 and having the source return suggestions for empty queries.</h6>
                <div id="default-suggestions">
                    <input class="typeahead form-control" type="text" placeholder="NFL Teams">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- row -->
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Multiple Datasets</h4>
                <h6 class="card-subtitle">Use multiple datasets like this</h6>
                <div id="multiple-datasets">
                    <input class="typeahead form-control" type="text" placeholder="NBA and NHL teams">
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Scrollable Dropdown Menu</h4>
                <h6 class="card-subtitle">You can use scrollable drowdown</h6>
                <div id="scrollable-dropdown-menu">
                    <input class="typeahead form-control" type="text" placeholder="Countries">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- row -->
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->