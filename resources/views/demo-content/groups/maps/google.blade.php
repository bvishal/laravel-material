@push('after-scripts')

    <!-- google maps api -->
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyCUBL-6KdclGJ2a_UpmB2LXvq7VOcPT7K4&sensor=true"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/gmaps/gmaps.min.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/gmaps/jquery.gmaps.js"></script>

@endpush

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Simple Basic Map</h4>
                <div id="gmaps-simple" class="gmaps"></div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Market with Info window</h4>
                <div id="markermap" class="gmaps"></div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Over Layer Map</h4>
                <div id="overlayermap" class="gmaps"></div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Polygonal Map</h4>
                <div id="polymap" class="gmaps"></div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Routes Map</h4>
                <div id="routesmap" class="gmaps"></div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Styled Map</h4>
                <div id="styledmap" class="gmaps"></div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->