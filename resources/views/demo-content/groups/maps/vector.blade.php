@push('before-styles')

    <!-- Vector CSS -->
    <link href="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" />

@endpush

@push('after-scripts')

    <!-- Vector map JavaScript -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/vectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/vectormap/jquery-jvectormap-in-mill.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/vectormap/jquery-jvectormap-us-aea-en.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/vectormap/jquery-jvectormap-uk-mill-en.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/vectormap/jquery-jvectormap-au-mill.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/vectormap/jvectormap.custom.js"></script>

@endpush

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">World Map</h4>
                <div id="world-map-markers" style="height: 700px"></div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">India</h4>
                <div id="india" style="height: 450px"></div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Usa</h4>
                <div id="usa" style="height: 450px"></div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Australia Map</h4>
                <div id="australia" style="height: 450px"></div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Uk Map</h4>
                <div id="uk" style="height: 450px"></div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->