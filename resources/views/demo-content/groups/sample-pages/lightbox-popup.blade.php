@push('before-styles')

    <!-- Popup CSS -->
    <link href="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">

@endpush

@push('after-scripts')

    <!-- Magnific popup JavaScript -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>

@endpush

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Single image lightbox</h4>
                <h6 class="card-subtitle">add like this <code>&lt;a class="image-popup-no-margins" href="..." title="title will be apear here"&gt;&lt;img src="..."/&gt;&lt;/a&gt;</code></h6>
                <div class="row m-t-30">
                    <div class="col-md-4">
                        <a class="image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img1.jpg" title="Caption. Can be aligned to any side and contain any HTML."> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img1.jpg" alt="image" class="img-responsive" /> </a>
                        <h6 class="m-t-10">Fits horizontally and vertically</h6> </div>
                    <div class="col-md-4">
                        <a class="image-popup-fit-width" href="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img2.jpg" title="This image fits only horizontally."> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img2.jpg" alt="image" class="img-responsive" /> </a>
                        <h6 class="m-t-10">Only horizontally</h6> </div>
                    <div class="col-md-4">
                        <a class="image-popup-no-margins" href="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img3.jpg"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img3.jpg" alt="image" class="img-responsive" /> </a>
                        <h6 class="m-t-10">Zoom animation, close on top-right.</h6> </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Lightbox gallery</h4>
                <h6 class="card-subtitle">just add code under class="popup-gallery".</h6>
                <div class="popup-gallery row m-t-30">
                    <div class="col-md-4">
                        <a href="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img4.jpg" title="Caption. Can be aligned to any side and contain any HTML."> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img4.jpg" class="img-responsive" alt="img" /> </a>
                    </div>
                    <div class="col-md-4">
                        <a href="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img5.jpg" title="This image fits only horizontally."> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img5.jpg" class="img-responsive" alt="img" /> </a>
                    </div>
                    <div class="col-md-4">
                        <a href="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img6.jpg"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img6.jpg" class="img-responsive" alt="img" /> </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Zoom gallery</h4>
                <h6 class="card-subtitle">just add code under class="zoom-gallery".</h6>
                <div class="zoom-gallery row m-t-30">
                    <div class="col-md-4">
                        <a href="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img1.jpg" title="Caption. Can be aligned to any side and contain any HTML."> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img4.jpg" class="img-responsive" alt="img" /> </a>
                    </div>
                    <div class="col-md-4">
                        <a href="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img2.jpg" title="This image fits only horizontally."> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img5.jpg" class="img-responsive" alt="img" /> </a>
                    </div>
                    <div class="col-md-4">
                        <a href="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img3.jpg"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img6.jpg" class="img-responsive" alt="img" /> </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="card-title">Popup with Youtube Video</h4>
                        <h6 class="card-subtitle">You can use youtube video with popup just add class <code>popup-youtube</code></h6>
                        <a class="popup-youtube btn btn-danger" href="www.youtube.com/watch?v=sK7riqg2mr4">Open YouTube video</a>
                    </div>
                    <div class="col-md-6">
                        <h4 class="card-title">Google map</h4>
                        <h6 class="card-subtitle">You can use Googlemap with popup just add class with <code>popup-gmaps</code></h6>
                        <a class="popup-gmaps btn btn-info" href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&amp;hl=en&amp;t=v&amp;hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom">Open Google Map</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div id="image-popups" class="row">
                    <div class="col-lg-2 col-md-4">
                        <a href="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img1.jpg" data-effect="mfp-zoom-in"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img1.jpg" class="img-responsive" />
                            <br/>Zoom</a>
                    </div>
                    <div class="col-lg-2 col-md-4">
                        <a href="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img2.jpg" data-effect="mfp-newspaper"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img2.jpg" class="img-responsive" />
                            <br/>Newspaper</a>
                    </div>
                    <div class="col-lg-2 col-md-4">
                        <a href="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img3.jpg" data-effect="mfp-move-horizontal"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img3.jpg" class="img-responsive" />
                            <br/>Horizontal move</a>
                    </div>
                    <div class="col-lg-2 col-md-4">
                        <a href="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img4.jpg" data-effect="mfp-move-from-top"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img4.jpg" class="img-responsive" />
                            <br/>Move from top</a>
                    </div>
                    <div class="col-lg-2 col-md-4">
                        <a href="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img5.jpg" data-effect="mfp-3d-unfold"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img5.jpg" class="img-responsive" />
                            <br/>3d unfold</a>
                    </div>
                    <div class="col-lg-2 col-md-4">
                        <a href="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img6.jpg" data-effect="mfp-zoom-out"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img5.jpg" class="img-responsive" />
                            <br/>Zoom-out</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->