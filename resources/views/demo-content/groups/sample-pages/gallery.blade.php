@push('before-styles')

    <!-- Popup CSS -->
    <link href="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">

@endpush

@push('after-scripts')

    <!-- Magnific popup JavaScript -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>

@endpush

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row el-element-overlay">
    <div class="col-md-12">
        <h4 class="card-title">Gallery page</h4>
        <h6 class="card-subtitle m-b-20 text-muted">you can make gallery like this</h6></div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img1.jpg" alt="user" />
                    <div class="el-overlay">
                        <ul class="el-info">
                            <li><a class="btn default btn-outline image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/1.jpg"><i class="icon-magnifier"></i></a></li>
                            <li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="el-card-content">
                    <h3 class="box-title">Project title</h3> <small>subtitle of project</small>
                    <br/> </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img2.jpg" alt="user" />
                    <div class="el-overlay">
                        <ul class="el-info">
                            <li><a class="btn default btn-outline image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/1.jpg"><i class="icon-magnifier"></i></a></li>
                            <li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="el-card-content">
                    <h3 class="box-title">Project title</h3> <small>subtitle of project</small>
                    <br/> </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img3.jpg" alt="user" />
                    <div class="el-overlay">
                        <ul class="el-info">
                            <li><a class="btn default btn-outline image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/1.jpg"><i class="icon-magnifier"></i></a></li>
                            <li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="el-card-content">
                    <h3 class="box-title">Project title</h3> <small>subtitle of project</small>
                    <br/> </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img4.jpg" alt="user" />
                    <div class="el-overlay">
                        <ul class="el-info">
                            <li><a class="btn default btn-outline image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/1.jpg"><i class="icon-magnifier"></i></a></li>
                            <li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="el-card-content">
                    <h3 class="box-title">Project title</h3> <small>subtitle of project</small>
                    <br/> </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img3.jpg" alt="user" />
                    <div class="el-overlay">
                        <ul class="el-info">
                            <li><a class="btn default btn-outline image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/1.jpg"><i class="icon-magnifier"></i></a></li>
                            <li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="el-card-content">
                    <h3 class="box-title">Project title</h3> <small>subtitle of project</small>
                    <br/> </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img5.jpg" alt="user" />
                    <div class="el-overlay">
                        <ul class="el-info">
                            <li><a class="btn default btn-outline image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/1.jpg"><i class="icon-magnifier"></i></a></li>
                            <li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="el-card-content">
                    <h3 class="box-title">Project title</h3> <small>subtitle of project</small>
                    <br/> </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img6.jpg" alt="user" />
                    <div class="el-overlay">
                        <ul class="el-info">
                            <li><a class="btn default btn-outline image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/1.jpg"><i class="icon-magnifier"></i></a></li>
                            <li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="el-card-content">
                    <h3 class="box-title">Project title</h3> <small>subtitle of project</small>
                    <br/> </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img1.jpg" alt="user" />
                    <div class="el-overlay">
                        <ul class="el-info">
                            <li><a class="btn default btn-outline image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/1.jpg"><i class="icon-magnifier"></i></a></li>
                            <li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="el-card-content">
                    <h3 class="box-title">Project title</h3> <small>subtitle of project</small>
                    <br/> </div>
            </div>
        </div>
    </div>
</div>
<div class="row m-t-40">
    <div class="col-md-12">
        <h4 class="card-title">Other Gallery </h4>
        <h6 class="card-subtitle m-b-20 text-muted">you can make gallery like this</h6> </div>
</div>
<div class="card-columns el-element-overlay">
    <div class="card">
        <div class="el-card-item">
            <div class="el-card-avatar el-overlay-1">
                <a class="image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img5.jpg"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img5.jpg" alt="user" /> </a>
            </div>
            <div class="el-card-content">
                <h3 class="box-title">Project title</h3> <small>subtitle of project</small>
                <br/> </div>
        </div>
    </div>
    <div class="card">
        <div class="el-card-item">
            <div class="el-card-avatar el-overlay-1">
                <a class="image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/1.jpg"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/1.jpg" alt="user" /> </a>
            </div>
            <div class="el-card-content">
                <h3 class="box-title">Project title</h3> <small>subtitle of project</small>
                <br/> </div>
        </div>
    </div>
    <div class="card">
        <div class="el-card-item">
            <div class="el-card-avatar el-overlay-1">
                <a class="image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/2.jpg"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/2.jpg" alt="user" /> </a>
            </div>
            <div class="el-card-content">
                <h3 class="box-title">Project title</h3> <small>subtitle of project</small>
                <br/> </div>
        </div>
    </div>
    <div class="card">
        <div class="el-card-item">
            <div class="el-card-avatar el-overlay-1">
                <a class="image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img4.jpg"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img4.jpg" alt="user" /> </a>
            </div>
            <div class="el-card-content">
                <h3 class="box-title">Project title</h3> <small>subtitle of project</small>
                <br/> </div>
        </div>
    </div>
    <div class="card">
        <div class="el-card-item">
            <div class="el-card-avatar el-overlay-1">
                <a class="image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img2.jpg"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img2.jpg" alt="user" /> </a>
            </div>
            <div class="el-card-content">
                <h3 class="box-title">Project title</h3> <small>subtitle of project</small>
                <br/> </div>
        </div>
    </div>
    <div class="card">
        <div class="el-card-item">
            <div class="el-card-avatar el-overlay-1">
                <a class="image-popup-vertical-fit" href="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/1.jpg"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img1.jpg" alt="user" /> </a>
            </div>
            <div class="el-card-content">
                <h3 class="box-title">Project title</h3> <small>subtitle of project</small>
                <br/> </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->