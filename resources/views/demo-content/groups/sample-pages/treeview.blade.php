@push('after-scripts')

    <!-- Treeview Plugin JavaScript -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/bootstrap-treeview-master/dist/bootstrap-treeview.min.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/bootstrap-treeview-master/dist/bootstrap-treeview-init.js"></script>

@endpush

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <h4 class="card-title">Default</h4>
                        <div id="treeview1" class=""></div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <h4 class="card-title">Collapsed</h4>
                        <div id="treeview2" class=""></div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <h4 class="card-title">Tags as Badges</h4>
                        <div id="treeview3" class=""></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <h4 class="card-title">Blue Theme</h4>
                        <div id="treeview4" class=""></div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <h4 class="card-title">Custom Icons</h4>
                        <div id="treeview5" class=""></div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <h4 class="card-title">Expanded</h4>
                        <div id="treeview6" class=""></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Searchable Tree</h4>
                <div class="row">
                    <div class="col-sm-4">
                        <h2>Input</h2>
                        <!-- <form> -->
                        <div class="form-group">
                            <label for="input-search" class="sr-only">Search Tree:</label>
                            <input type="input" class="form-control" id="input-search" placeholder="Type to search..." value="">
                        </div>
                        <div class="checkbox checkbox-info">
                            <input type="checkbox" class="checkbox" id="chk-ignore-case" value="false">
                            <label for="chk-ignore-case">Ignore Case</label>
                        </div>
                        <div class="checkbox checkbox-info">
                            <input type="checkbox" class="checkbox" id="chk-exact-match" value="false">
                            <label for="chk-exact-match">Exact Match</label>
                        </div>
                        <div class="checkbox checkbox-info">
                            <input type="checkbox" class="checkbox" id="chk-reveal-results" value="false">
                            <label for="chk-reveal-results">Reveal Results</label>
                        </div>
                        <button type="button" class="btn btn-success" id="btn-search">Search</button>
                        <button type="button" class="btn btn-default" id="btn-clear-search">Clear</button>
                        <!-- </form> -->
                    </div>
                    <div class="col-sm-4">
                        <h2>Tree</h2>
                        <div id="treeview-searchable" class=""></div>
                    </div>
                    <div class="col-sm-4">
                        <h2>Results</h2>
                        <div id="search-output"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->