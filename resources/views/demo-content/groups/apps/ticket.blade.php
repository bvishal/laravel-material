@push('before-styles')
    <!-- Footable CSS -->
    <link href="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/footable/css/footable.core.css" rel="stylesheet">
    <link href="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
@endpush

@push('after-scripts')
    <!-- Footable -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/footable/js/footable.all.min.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
    <!--FooTable init-->
    <script src="/vendor/wrappixel/material-pro/4.1.0/material/js/footable-init.js"></script>
@endpush

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Support Ticket List</h4>
                <h6 class="card-subtitle">List of ticket opend by customers</h6>
                <div class="row m-t-40">
                    <!-- Column -->
                    <div class="col-md-6 col-lg-3 col-xlg-3">
                        <div class="card card-inverse card-info">
                            <div class="box bg-info text-center">
                                <h1 class="font-light text-white">2,064</h1>
                                <h6 class="text-white">Total Tickets</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-3 col-xlg-3">
                        <div class="card card-primary card-inverse">
                            <div class="box text-center">
                                <h1 class="font-light text-white">1,738</h1>
                                <h6 class="text-white">Responded</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-3 col-xlg-3">
                        <div class="card card-inverse card-success">
                            <div class="box text-center">
                                <h1 class="font-light text-white">1100</h1>
                                <h6 class="text-white">Resolve</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-3 col-xlg-3">
                        <div class="card card-inverse card-dark">
                            <div class="box text-center">
                                <h1 class="font-light text-white">964</h1>
                                <h6 class="text-white">Pending</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <div class="table-responsive">
                    <table id="demo-foo-addrow" class="table m-t-30 table-hover no-wrap contact-list" data-page-size="10">
                        <thead>
                        <tr>
                            <th>ID #</th>
                            <th>Opened By</th>
                            <th>Cust. Email</th>
                            <th>Sbuject</th>
                            <th>Status</th>
                            <th>Assign to</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1011</td>
                            <td>
                                <a href="javascript:void(0)"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/1.jpg" alt="user" class="img-circle" /> Genelia Deshmukh</a>
                            </td>
                            <td>genelia@gmail.com</td>
                            <td>How to customize the template?</td>
                            <td><span class="label label-warning">New</span> </td>
                            <td>Johnathon</td>
                            <td>14-10-2017</td>
                            <td>
                                <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td>1224</td>
                            <td>
                                <a href="javascript:void(0)"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/2.jpg" alt="user" class="img-circle" /> Ritesh Deshmukh</a>
                            </td>
                            <td>ritesh@gmail.com</td>
                            <td>How to change colors</td>
                            <td><span class="label label-success">Complete</span> </td>
                            <td>Salman khan</td>
                            <td>13-10-2017</td>
                            <td>
                                <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td>1024</td>
                            <td>
                                <a href="javascript:void(0)"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/3.jpg" alt="user" class="img-circle" /> Govinda Mauli</a>
                            </td>
                            <td>govinda@gmail.com</td>
                            <td>How to set Horizontal nav</td>
                            <td><span class="label label-success">Complete</span> </td>
                            <td>Hritik Roshan</td>
                            <td>13-10-2017</td>
                            <td>
                                <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td>2124</td>
                            <td>
                                <a href="javascript:void(0)"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/4.jpg" alt="user" class="img-circle" /> Raja Mauli</a>
                            </td>
                            <td>bahubali@gmail.com</td>
                            <td>How this will connect with bahubali</td>
                            <td><span class="label label-inverse">Pending</span> </td>
                            <td>Hritik Roshan</td>
                            <td>12-10-2017</td>
                            <td>
                                <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td>3124</td>
                            <td>
                                <a href="javascript:void(0)"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/5.jpg" alt="user" class="img-circle" /> Rana Dagubati</a>
                            </td>
                            <td>ranabati@gmail.com</td>
                            <td>How to set navigation</td>
                            <td><span class="label label-success">Complete</span> </td>
                            <td>Hritik Roshan</td>
                            <td>12-10-2017</td>
                            <td>
                                <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td>1611</td>
                            <td>
                                <a href="javascript:void(0)"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/6.jpg" alt="user" class="img-circle" /> Tony Deshmukh</a>
                            </td>
                            <td>genelia@gmail.com</td>
                            <td>How to customize the template?</td>
                            <td><span class="label label-warning">New</span> </td>
                            <td>Johnathon</td>
                            <td>14-10-2017</td>
                            <td>
                                <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td>4224</td>
                            <td>
                                <a href="javascript:void(0)"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/7.jpg" alt="user" class="img-circle" /> Ritesh Deshmukh</a>
                            </td>
                            <td>ritesh@gmail.com</td>
                            <td>How to change colors</td>
                            <td><span class="label label-success">Complete</span> </td>
                            <td>Salman khan</td>
                            <td>13-10-2017</td>
                            <td>
                                <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td>2524</td>
                            <td>
                                <a href="javascript:void(0)"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/3.jpg" alt="user" class="img-circle" /> Govinda Mauli</a>
                            </td>
                            <td>govinda@gmail.com</td>
                            <td>How to set Horizontal nav</td>
                            <td><span class="label label-success">Complete</span> </td>
                            <td>Hritik Roshan</td>
                            <td>13-10-2017</td>
                            <td>
                                <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td>7524</td>
                            <td>
                                <a href="javascript:void(0)"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/4.jpg" alt="user" class="img-circle" /> Raja Mauli</a>
                            </td>
                            <td>bahubali@gmail.com</td>
                            <td>How this will connect with bahubali</td>
                            <td><span class="label label-inverse">Pending</span> </td>
                            <td>Hritik Roshan</td>
                            <td>12-10-2017</td>
                            <td>
                                <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td>4124</td>
                            <td>
                                <a href="javascript:void(0)"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/5.jpg" alt="user" class="img-circle" /> Rana Dagubati</a>
                            </td>
                            <td>ranabati@gmail.com</td>
                            <td>How to set navigation</td>
                            <td><span class="label label-success">Complete</span> </td>
                            <td>Hritik Roshan</td>
                            <td>12-10-2017</td>
                            <td>
                                <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td>1011</td>
                            <td>
                                <a href="javascript:void(0)"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/8.jpg" alt="user" class="img-circle" /> Ama Deshmukh</a>
                            </td>
                            <td>genelia@gmail.com</td>
                            <td>How to customize the template?</td>
                            <td><span class="label label-warning">New</span> </td>
                            <td>Johnathon</td>
                            <td>14-10-2017</td>
                            <td>
                                <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td>1224</td>
                            <td>
                                <a href="javascript:void(0)"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/7.jpg" alt="user" class="img-circle" /> adfh abraham</a>
                            </td>
                            <td>ritesh@gmail.com</td>
                            <td>How to change colors</td>
                            <td><span class="label label-success">Complete</span> </td>
                            <td>Salman khan</td>
                            <td>13-10-2017</td>
                            <td>
                                <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td>8024</td>
                            <td>
                                <a href="javascript:void(0)"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/6.jpg" alt="user" class="img-circle" /> atest adg</a>
                            </td>
                            <td>govinda@gmail.com</td>
                            <td>How to set Horizontal nav</td>
                            <td><span class="label label-success">Complete</span> </td>
                            <td>Hritik Roshan</td>
                            <td>13-10-2017</td>
                            <td>
                                <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td>5124</td>
                            <td>
                                <a href="javascript:void(0)"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/5.jpg" alt="user" class="img-circle" /> Raja Mauli</a>
                            </td>
                            <td>bahubali@gmail.com</td>
                            <td>How this will connect with bahubali</td>
                            <td><span class="label label-inverse">Pending</span> </td>
                            <td>Hritik Roshan</td>
                            <td>12-10-2017</td>
                            <td>
                                <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td>3144</td>
                            <td>
                                <a href="javascript:void(0)"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/4.jpg" alt="user" class="img-circle" /> Rana Dagubati</a>
                            </td>
                            <td>ranabati@gmail.com</td>
                            <td>How to set navigation</td>
                            <td><span class="label label-success">Complete</span> </td>
                            <td>Hritik Roshan</td>
                            <td>12-10-2017</td>
                            <td>
                                <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td>1621</td>
                            <td>
                                <a href="javascript:void(0)"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/3.jpg" alt="user" class="img-circle" /> Tony Deshmukh</a>
                            </td>
                            <td>genelia@gmail.com</td>
                            <td>How to customize the template?</td>
                            <td><span class="label label-warning">New</span> </td>
                            <td>Johnathon</td>
                            <td>14-10-2017</td>
                            <td>
                                <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td>4724</td>
                            <td>
                                <a href="javascript:void(0)"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/2.jpg" alt="user" class="img-circle" /> Ritesh Deshmukh</a>
                            </td>
                            <td>ritesh@gmail.com</td>
                            <td>How to change colors</td>
                            <td><span class="label label-success">Complete</span> </td>
                            <td>Salman khan</td>
                            <td>13-10-2017</td>
                            <td>
                                <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td>2594</td>
                            <td>
                                <a href="javascript:void(0)"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/1.jpg" alt="user" class="img-circle" /> Govinda Mauli</a>
                            </td>
                            <td>govinda@gmail.com</td>
                            <td>How to set Horizontal nav</td>
                            <td><span class="label label-success">Complete</span> </td>
                            <td>Hritik Roshan</td>
                            <td>13-10-2017</td>
                            <td>
                                <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td>7524</td>
                            <td>
                                <a href="javascript:void(0)"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/4.jpg" alt="user" class="img-circle" /> Raja Mauli</a>
                            </td>
                            <td>bahubali@gmail.com</td>
                            <td>How this will connect with bahubali</td>
                            <td><span class="label label-inverse">Pending</span> </td>
                            <td>Hritik Roshan</td>
                            <td>12-10-2017</td>
                            <td>
                                <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td>4124</td>
                            <td>
                                <a href="javascript:void(0)"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/5.jpg" alt="user" class="img-circle" /> Rana Dagubati</a>
                            </td>
                            <td>ranabati@gmail.com</td>
                            <td>How to set navigation</td>
                            <td><span class="label label-success">Complete</span> </td>
                            <td>Hritik Roshan</td>
                            <td>12-10-2017</td>
                            <td>
                                <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="2">
                                <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#add-contact">Add New Contact</button>
                            </td>
                            <div id="add-contact" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel">Add New Contact</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        </div>
                                        <div class="modal-body">
                                            <from class="form-horizontal form-material">
                                                <div class="form-group">
                                                    <div class="col-md-12 m-b-20">
                                                        <input type="text" class="form-control" placeholder="Type name"> </div>
                                                    <div class="col-md-12 m-b-20">
                                                        <input type="text" class="form-control" placeholder="Email"> </div>
                                                    <div class="col-md-12 m-b-20">
                                                        <input type="text" class="form-control" placeholder="Phone"> </div>
                                                    <div class="col-md-12 m-b-20">
                                                        <input type="text" class="form-control" placeholder="Designation"> </div>
                                                    <div class="col-md-12 m-b-20">
                                                        <input type="text" class="form-control" placeholder="Age"> </div>
                                                    <div class="col-md-12 m-b-20">
                                                        <input type="text" class="form-control" placeholder="Date of joining"> </div>
                                                    <div class="col-md-12 m-b-20">
                                                        <input type="text" class="form-control" placeholder="Salary"> </div>
                                                    <div class="col-md-12 m-b-20">
                                                        <div class="fileupload btn btn-danger btn-rounded waves-effect waves-light"><span><i class="ion-upload m-r-5"></i>Upload Contact Image</span>
                                                            <input type="file" class="upload"> </div>
                                                    </div>
                                                </div>
                                            </from>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Save</button>
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <td colspan="6">
                                <div class="text-right">
                                    <ul class="pagination"> </ul>
                                </div>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>