@push('after-styles')
    <!-- chartist CSS -->
    <link href="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
    <link href="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">

    <!--c3 CSS -->
    <link href="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/c3-master/c3.min.css" rel="stylesheet">
@endpush

@push('after-scripts')

    <!-- chartist chart -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/chartist-js/dist/chartist.min.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>


    <!--c3 JavaScript -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/d3/d3.min.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/c3-master/c3.min.js"></script>

    {{--<script src="/vendor/wrappixel/material-pro/4.1.0/material/js/dashboard1.js"></script>--}}
    <script>

        var chart2Data = @json($chart2Data);

        var chart2 = new Chartist.Bar('.amp-pxl', {
            labels: chart2Data.labels,
            series: chart2Data.series
        }, {
            axisX: {
                // On the x-axis start means top and end means bottom
                position: 'end',
                showGrid: false
            },
            axisY: {
                // On the y-axis start means left and end means right
                position: 'start'
            },
            high: chart2Data.high,
            low: chart2Data.low,
            plugins: [
                Chartist.plugins.tooltip()
            ]
        });

        var chartVisitorsData = @json($chartVisitorsData);
        var chartVisitors = c3.generate({
            bindto: '#visitor',
            data: {
                columns: chartVisitorsData.columns,
                type : 'donut'
            },
            donut: {
                label: {
                    show: false
                },
                title: "Our visitor",
                width:20,
            },
            legend: {
                hide: true
            },
            color: {
                pattern: chartVisitorsData.pattern
            }
        });

        var chartCampaignData = @json($chartCampaignData);
        var chartCampaign = new Chartist.Line('.campaign', {
                labels: chartCampaignData.labels,
                series: chartCampaignData.series },
            {
                high: chartCampaignData.high,
                low: chartCampaignData.low,
                showArea: true,
                fullWidth: true,
                plugins: [
                    Chartist.plugins.tooltip()
                ],
                axisY: {
                    onlyInteger: true
                    , scaleMinSpace: 40
                    , offset: 20
                    , labelInterpolationFnc: function (value) {
                        return (value / 1) + 'k';
                    }
                },
            });
        // Offset x1 a tiny amount so that the straight stroke gets a bounding box
        // Straight lines don't get a bounding box
        // Last remark on -> http://www.w3.org/TR/SVG11/coords.html#ObjectBoundingBox
        chartCampaign.on('draw', function(ctx) {
            if(ctx.type === 'area') {
                ctx.element.attr({
                    x1: ctx.x1 + 0.001
                });
            }
        });

        // Create the gradient definition on created event (always after chart re-render)
        chartCampaign.on('created', function(ctx) {
            var defs = ctx.svg.elem('defs');
            defs.elem('linearGradient', {
                id: 'gradient',
                x1: 0,
                y1: 1,
                x2: 0,
                y2: 0
            }).elem('stop', {
                offset: 0,
                'stop-color': 'rgba(255, 255, 255, 1)'
            }).parent().elem('stop', {
                offset: 1,
                'stop-color': 'rgba(38, 198, 218, 1)'
            });
        });


        // ==============================================================
        // This is for the animation
        // ==============================================================
        var chart = [chart2, chartCampaign];
        for (var i = 0; i < chart.length; i++) {
            chart[i].on('draw', function(data) {
                if (data.type === 'line' || data.type === 'area') {
                    data.element.animate({
                        d: {
                            begin: 500 * data.index,
                            dur: 500,
                            from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                            to: data.path.clone().stringify(),
                            easing: Chartist.Svg.Easing.easeInOutElastic
                        }
                    });
                }
                if (data.type === 'bar') {
                    data.element.animate({
                        y2: {
                            dur: 500,
                            from: data.y1,
                            to: data.y2,
                            easing: Chartist.Svg.Easing.easeInOutElastic
                        },
                        opacity: {
                            dur: 500,
                            from: 0,
                            to: 1,
                            easing: Chartist.Svg.Easing.easeInOutElastic
                        }
                    });
                }
            });
        }
        // ==============================================================
        // Our visitor
        // ==============================================================

        // ==============================================================
        // Badnwidth usage
        // ==============================================================
        new Chartist.Line('.usage', {
            labels: ['0', '4', '8', '12', '16', '20', '24', '30']
            , series: [
                [5, 0, 12, 1, 8, 3, 12, 15]

            ]
        }, {
            high:10
            , low: 0
            , showArea: true
            , fullWidth: true
            , plugins: [
                Chartist.plugins.tooltip()
            ], // As this is axis specific we need to tell Chartist to use whole numbers only on the concerned axis
            axisY: {
                onlyInteger: true
                , offset: 20
                , showLabel: false
                , showGrid: false
                , labelInterpolationFnc: function (value) {
                    return (value / 1) + 'k';
                }
            }
            , axisX: {
                showLabel: false
                , divisor: 1
                , showGrid: false
                , offset: 0
            }
        });


        // ==============================================================
        // Download count
        // ==============================================================
        var sparklineLogin = function () {
            $('.spark-count').sparkline([4, 5, 0, 10, 9, 12, 4, 9, 4, 5, 3, 10, 9, 12, 10, 9, 12, 4, 9], {
                type: 'bar'
                , width: '100%'
                , height: '70'
                , barWidth: '2'
                , resize: true
                , barSpacing: '6'
                , barColor: 'rgba(255, 255, 255, 0.3)'
            });

        }
        var sparkResize;

        sparklineLogin();
    </script>
@endpush

@section('content')


    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div class="row">
        <!-- Column -->
        <div class="col-lg-8 col-md-7">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="d-flex flex-wrap">
                                <div>
                                    <h3 class="card-title">{{ $chart2Data['title'] or 'Title' }}</h3>
                                    <h6 class="card-subtitle">{{ $chart2Data['subtitle'] or 'Subtitle' }}</h6> </div>
                                <div class="ml-auto">
                                    <ul class="list-inline">
                                        @forelse($chart2Data['seriesTitles'] as $key=>$serieTitle)
                                            <li>
                                                <h6 class="text-muted {{ $chart2Data['seriesClasses'][$key] }}">
                                                    <i class="fa fa-circle font-10 m-r-10 "></i>
                                                    {{ $serieTitle }}
                                                </h6>
                                            </li>
                                        @empty
                                            <li>
                                                <h6 class="text-muted text-danger">
                                                    <i class="fa fa-circle font-10 m-r-10 "></i>
                                                    No data found
                                                </h6>
                                            </li>
                                        @endforelse
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="amp-pxl" style="height: 360px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-5">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title">{{ $chartVisitorsData['title'] or 'Title' }} </h3>
                    <h6 class="card-subtitle">{{ $chartVisitorsData['subtitle'] or 'Subtitle' }} </h6>
                    <div id="visitor" style="height:290px; width:100%;"></div>
                </div>
                <div>
                    <hr class="m-t-0 m-b-0">
                </div>
                <div class="card-body text-center ">
                    <ul class="list-inline m-b-0">
                        @foreach ($chartVisitorsData['columns'] as $column)
                            <li>
                                <h6 class="text-muted" style="color: {!! $chartVisitorsData['pattern'][$loop->index] !!} !important;"><i class="fa fa-circle font-10 m-r-10 "></i>{!! $column[0] !!}</h6> </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->
    <!-- Row -->
    <div class="row">
        <!-- Column -->
        <div class="col-lg-4 col-xlg-3 col-md-5">
            <div class="card blog-widget">
                <div class="card-body">
                    <div class="blog-image"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img1.jpg" alt="img" class="img-responsive" /></div>
                    <h3>Business development new rules for 2017</h3>
                    <label class="label label-rounded label-success">Technology</label>
                    <p class="m-t-20 m-b-20">
                        Lorem ipsum dolor sit amet, this is a consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                    </p>
                    <div class="d-flex">
                        <div class="read"><a href="javascript:void(0)" class="link font-medium">Read More</a></div>
                        <div class="ml-auto">
                            <a href="javascript:void(0)" class="link m-r-10 " data-toggle="tooltip" title="Like"><i class="mdi mdi-heart-outline"></i></a> <a href="javascript:void(0)" class="link" data-toggle="tooltip" title="Share"><i class="mdi mdi-share-variant"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex flex-wrap">
                        <div>
                            <h3 class="card-title">{{ $chartCampaignData['title'] or 'Title' }}</h3>
                            <h6 class="card-subtitle">{{ $chartCampaignData['subtitle'] or 'Subtitle' }}</h6>
                        </div>
                        <div class="ml-auto align-self-center">
                            <ul class="list-inline m-b-0">
                                <li>
                                    <h6 class="text-muted text-success"><i class="fa fa-circle font-10 m-r-10 "></i>Open Rate</h6> </li>
                                <li>
                                    <h6 class="text-muted text-info"><i class="fa fa-circle font-10 m-r-10"></i>Recurring Payments</h6> </li>
                            </ul>
                        </div>
                    </div>
                    <div class="campaign ct-charts"></div>
                    <div class="row text-center">
                        <div class="col-lg-4 col-md-4 m-t-20">
                            <h1 class="m-b-0 font-light">5098</h1><small>Total Sent</small></div>
                        <div class="col-lg-4 col-md-4 m-t-20">
                            <h1 class="m-b-0 font-light">4156</h1><small>Mail Open Rate</small></div>
                        <div class="col-lg-4 col-md-4 m-t-20">
                            <h1 class="m-b-0 font-light">1369</h1><small>Click Rate</small></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->
    <div class="row">
        <!-- Column -->
        <div class="col-lg-4 col-md-4">
            <div class="card card-inverse card-primary">
                <div class="card-body">
                    <div class="d-flex">
                        <div class="m-r-20 align-self-center">
                            <h1 class="text-white"><i class="ti-pie-chart"></i></h1></div>
                        <div>
                            <h3 class="card-title">Bandwidth usage</h3>
                            <h6 class="card-subtitle">March  2017</h6> </div>
                    </div>
                    <div class="row">
                        <div class="col-4 align-self-center">
                            <h2 class="font-light text-white">50 GB</h2>
                        </div>
                        <div class="col-8 p-t-10 p-b-20 align-self-center">
                            <div class="usage chartist-chart" style="height:65px"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-4 col-md-4">
            <div class="card card-inverse card-success">
                <div class="card-body">
                    <div class="d-flex">
                        <div class="m-r-20 align-self-center">
                            <h1 class="text-white"><i class="icon-cloud-download"></i></h1></div>
                        <div>
                            <h3 class="card-title">Download count</h3>
                            <h6 class="card-subtitle">March  2017</h6> </div>
                    </div>
                    <div class="row">
                        <div class="col-4 align-self-center">
                            <h2 class="font-light text-white">35487</h2>
                        </div>
                        <div class="col-8 p-t-10 p-b-20 text-right">
                            <div class="spark-count" style="height:65px"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-4 col-md-4">
            <div class="card">
                <img class="" src="/vendor/wrappixel/material-pro/4.1.0/assets/images/background/weatherbg.jpg" alt="Card image cap">
                <div class="card-img-overlay" style="height:110px;">
                    <h3 class="card-title text-white m-b-0 dl">New Delhi</h3>
                    <small class="card-text text-white font-light">Sunday 15 march</small>
                </div>
                <div class="card-body weather-small">
                    <div class="row">
                        <div class="col-8 b-r align-self-center">
                            <div class="d-flex">
                                <div class="display-6 text-info"><i class="wi wi-day-rain-wind"></i></div>
                                <div class="m-l-20">
                                    <h1 class="font-light text-info m-b-0">32<sup>0</sup></h1>
                                    <small>Sunny Rainy day</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-4 text-center">
                            <h1 class="font-light m-b-0">25<sup>0</sup></h1>
                            <small>Tonight</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
    <!-- Row -->
    <!-- Row -->
    <div class="row">
        <!-- Column -->
        <div class="col-lg-4 col-xlg-3 col-md-5">
            <!-- Column -->
            <div class="card">
                <img class="card-img-top" src="/vendor/wrappixel/material-pro/4.1.0/assets/images/background/profile-bg.jpg" alt="Card image cap">
                <div class="card-body little-profile text-center">
                    <div class="pro-img"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/4.jpg" alt="user" /></div>
                    <h3 class="m-b-0">Angela Dominic</h3>
                    <p>Web Designer &amp; Developer</p>
                    <a href="javascript:void(0)" class="m-t-10 waves-effect waves-dark btn btn-primary btn-md btn-rounded">Follow</a>
                    <div class="row text-center m-t-20">
                        <div class="col-lg-4 col-md-4 m-t-20">
                            <h3 class="m-b-0 font-light">1099</h3><small>Articles</small></div>
                        <div class="col-lg-4 col-md-4 m-t-20">
                            <h3 class="m-b-0 font-light">23,469</h3><small>Followers</small></div>
                        <div class="col-lg-4 col-md-4 m-t-20">
                            <h3 class="m-b-0 font-light">6035</h3><small>Following</small></div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="card">
                <div class="card-body bg-info">
                    <h4 class="text-white card-title">My Contacts</h4>
                    <h6 class="card-subtitle text-white m-b-0 op-5">Checkout my contacts here</h6>
                </div>
                <div class="card-body">
                    <div class="message-box contact-box">
                        <h2 class="add-ct-btn"><button type="button" class="btn btn-circle btn-lg btn-success waves-effect waves-dark">+</button></h2>
                        <div class="message-widget contact-widget">
                            <!-- Message -->
                            <a href="#">
                                <div class="user-img"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/1.jpg" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                                <div class="mail-contnet">
                                    <h5>Pavan kumar</h5> <span class="mail-desc">info@wrappixel.com</span></div>
                            </a>
                            <!-- Message -->
                            <a href="#">
                                <div class="user-img"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/2.jpg" alt="user" class="img-circle"> <span class="profile-status busy pull-right"></span> </div>
                                <div class="mail-contnet">
                                    <h5>Sonu Nigam</h5> <span class="mail-desc">pamela1987@gmail.com</span></div>
                            </a>
                            <!-- Message -->
                            <a href="#">
                                <div class="user-img"> <span class="round">A</span> <span class="profile-status away pull-right"></span> </div>
                                <div class="mail-contnet">
                                    <h5>Arijit Sinh</h5> <span class="mail-desc">cruise1298.fiplip@gmail.com</span></div>
                            </a>
                            <!-- Message -->
                            <a href="#">
                                <div class="user-img"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/4.jpg" alt="user" class="img-circle"> <span class="profile-status offline pull-right"></span> </div>
                                <div class="mail-contnet">
                                    <h5>Pavan kumar</h5> <span class="mail-desc">kat@gmail.com</span></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs profile-tab" role="tablist">
                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab">Activity</a> </li>
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile" role="tab">Profile</a> </li>
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab">Settings</a> </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="home" role="tabpanel">
                        <div class="card-body">
                            <div class="profiletimeline">
                                <div class="sl-item">
                                    <div class="sl-left"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/1.jpg" alt="user" class="img-circle"> </div>
                                    <div class="sl-right">
                                        <div><a href="#" class="link">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                            <p>assign a new task <a href="#"> Design weblayout</a></p>
                                            <div class="row">
                                                <div class="col-lg-3 col-md-6 m-b-20"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img1.jpg" alt="user" class="img-responsive radius"></div>
                                                <div class="col-lg-3 col-md-6 m-b-20"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img2.jpg" alt="user" class="img-responsive radius"></div>
                                                <div class="col-lg-3 col-md-6 m-b-20"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img3.jpg" alt="user" class="img-responsive radius"></div>
                                                <div class="col-lg-3 col-md-6 m-b-20"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img4.jpg" alt="user" class="img-responsive radius"></div>
                                            </div>
                                            <div class="like-comm"> <a href="javascript:void(0)" class="link m-r-10">2 comment</a> <a href="javascript:void(0)" class="link m-r-10"><i class="fa fa-heart text-danger"></i> 5 Love</a> </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="sl-item">
                                    <div class="sl-left"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/2.jpg" alt="user" class="img-circle"> </div>
                                    <div class="sl-right">
                                        <div> <a href="#" class="link">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                            <div class="m-t-20 row">
                                                <div class="col-md-3 col-xs-12"><img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img1.jpg" alt="user" class="img-responsive radius"></div>
                                                <div class="col-md-9 col-xs-12">
                                                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. </p> <a href="#" class="btn btn-success"> Design weblayout</a></div>
                                            </div>
                                            <div class="like-comm m-t-20"> <a href="javascript:void(0)" class="link m-r-10">2 comment</a> <a href="javascript:void(0)" class="link m-r-10"><i class="fa fa-heart text-danger"></i> 5 Love</a> </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="sl-item">
                                    <div class="sl-left"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/3.jpg" alt="user" class="img-circle"> </div>
                                    <div class="sl-right">
                                        <div><a href="#" class="link">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                            <p class="m-t-10"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper </p>
                                        </div>
                                        <div class="like-comm m-t-20"> <a href="javascript:void(0)" class="link m-r-10">2 comment</a> <a href="javascript:void(0)" class="link m-r-10"><i class="fa fa-heart text-danger"></i> 5 Love</a> </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="sl-item">
                                    <div class="sl-left"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/4.jpg" alt="user" class="img-circle"> </div>
                                    <div class="sl-right">
                                        <div><a href="#" class="link">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                            <blockquote class="m-t-10">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
                                            </blockquote>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--second tab-->
                    <div class="tab-pane" id="profile" role="tabpanel">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Full Name</strong>
                                    <br>
                                    <p class="text-muted">Johnathan Deo</p>
                                </div>
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Mobile</strong>
                                    <br>
                                    <p class="text-muted">(123) 456 7890</p>
                                </div>
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
                                    <br>
                                    <p class="text-muted">johnathan@admin.com</p>
                                </div>
                                <div class="col-md-3 col-xs-6"> <strong>Location</strong>
                                    <br>
                                    <p class="text-muted">London</p>
                                </div>
                            </div>
                            <hr>
                            <p class="m-t-30">Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</p>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries </p>
                            <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                            <h4 class="font-medium m-t-30">Skill Set</h4>
                            <hr>
                            <h5 class="m-t-30">Wordpress <span class="pull-right">80%</span></h5>
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%; height:6px;"> <span class="sr-only">50% Complete</span> </div>
                            </div>
                            <h5 class="m-t-30">HTML 5 <span class="pull-right">90%</span></h5>
                            <div class="progress">
                                <div class="progress-bar bg-info" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width:90%; height:6px;"> <span class="sr-only">50% Complete</span> </div>
                            </div>
                            <h5 class="m-t-30">jQuery <span class="pull-right">50%</span></h5>
                            <div class="progress">
                                <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%; height:6px;"> <span class="sr-only">50% Complete</span> </div>
                            </div>
                            <h5 class="m-t-30">Photoshop <span class="pull-right">70%</span></h5>
                            <div class="progress">
                                <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%; height:6px;"> <span class="sr-only">50% Complete</span> </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="settings" role="tabpanel">
                        <div class="card-body">
                            <form class="form-horizontal form-material">
                                <div class="form-group">
                                    <label class="col-md-12">Full Name</label>
                                    <div class="col-md-12">
                                        <input type="text" placeholder="Johnathan Doe" class="form-control form-control-line">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="example-email" class="col-md-12">Email</label>
                                    <div class="col-md-12">
                                        <input type="email" placeholder="johnathan@admin.com" class="form-control form-control-line" name="example-email" id="example-email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Password</label>
                                    <div class="col-md-12">
                                        <input type="password" value="password" class="form-control form-control-line">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Phone No</label>
                                    <div class="col-md-12">
                                        <input type="text" placeholder="123 456 7890" class="form-control form-control-line">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Message</label>
                                    <div class="col-md-12">
                                        <textarea rows="5" class="form-control form-control-line"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12">Select Country</label>
                                    <div class="col-sm-12">
                                        <select class="form-control form-control-line">
                                            <option>London</option>
                                            <option>India</option>
                                            <option>Usa</option>
                                            <option>Canada</option>
                                            <option>Thailand</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button class="btn btn-success">Update Profile</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->


@endsection
