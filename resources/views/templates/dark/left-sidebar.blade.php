<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User profile -->
        <div class="user-profile" style="background: url(/vendor/wrappixel/material-pro/4.1.0/assets/images/background/user-info.jpg) no-repeat;">
            <!-- User profile image -->
            <div class="profile-img"> <img src="/vendor/wrappixel/material-pro/4.1.0/assets/images/users/profile.png" alt="user" /> </div>
            <!-- User profile text-->
            <div class="profile-text"> <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">Markarn Doe</a>
                <div class="dropdown-menu animated flipInY"> <a href="#" class="dropdown-item"><i class="ti-user"></i> My Profile</a> <a href="#" class="dropdown-item"><i class="ti-wallet"></i> My Balance</a> <a href="#" class="dropdown-item"><i class="ti-email"></i> Inbox</a>
                    <div class="dropdown-divider"></div> <a href="#" class="dropdown-item"><i class="ti-settings"></i> Account Setting</a>
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('logout') }}"
                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                    class="dropdown-item"><i class="fa fa-power-off"></i> Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
        <!-- End User profile text-->
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">

                <li>
                    <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ url('/dark/php-to-js-vars') }}">PHP to JS vars</a></li>
                    </ul>
                </li>

                <li>
                    <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">Starter Pages </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ url('/dark/home-one') }}">Home 1</a></li>
                        <li><a href="{{ url('/dark/home-two') }}">Home 2</a></li>
                    </ul>
                </li>

                <li>
                    <a href="{{ url('/dark/blade-components') }}" aria-expanded="false"><i class="mdi mdi-chart-bubble"></i><span class="hide-menu">Blade Components </span></a>
                </li>

                <li>
                    <a href="{{ url('/dark/blade-loops') }}" aria-expanded="false"><i class="mdi mdi-widgets"></i><span class="hide-menu">Blade Loops </span></a>
                </li>

                <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu"> Layouts </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('dark.demos.boxed') }}">Boxed</a></li>
                        <li><a href="{{ route('dark.demos.logocenter') }}">Logo Center</a></li>
                        <li><a href="{{ route('dark.demos.singlecolumn') }}">Single Column</a></li>
                        <li><a href="{{ route('dark.demos.fixheader') }}">Fix header</a></li>
                        <li><a href="{{ route('dark.demos.fixsidebar') }}">Fix sidebar</a></li>
                        <li><a href="{{ route('dark.demos.fixheadersidebar') }}">Fix header & Sidebar</a></li>
                    </ul>
                </li>

                @inject('menuItems', 'sidebarmenuservice')

                @include('templates.application.components.menu-items-vertical',
                [ 'items' => $menuItems->getMenuItems('dark') ])

            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
    <!-- Bottom points-->
    <div class="sidebar-footer">
        <!-- item--><a href="" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a>
        <!-- item--><a href="" class="link" data-toggle="tooltip" title="Email"><i class="mdi mdi-gmail"></i></a>
        <!-- item--><a href="" class="link" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a> </div>
    <!-- End Bottom points-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->