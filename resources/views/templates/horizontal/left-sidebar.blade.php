<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">

                <li>
                    <a class="has-arrow" href="#" aria-expanded="false">
                        <i class="mdi mdi-gauge"></i>
                        <span class="hide-menu">Dashboard </span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ url('/horizontal/php-to-js-vars') }}">PHP to JS vars</a></li>
                    </ul>
                </li>

                <li>
                    <a class="has-arrow" href="#" aria-expanded="false">
                        <i class="mdi mdi-home"></i>
                        <span class="hide-menu">Starter Pages </span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ url('/horizontal/home-one') }}">Home 1</a></li>
                        <li><a href="{{ url('/horizontal/home-two') }}">Home 2</a></li>
                    </ul>
                </li>

                <li>
                    <a href="{{ url('/horizontal/blade-components') }}" aria-expanded="false">
                        <i class="mdi mdi-chart-bubble"></i>
                        <span class="hide-menu">Blade Components </span>
                    </a>
                </li>

                <li>
                    <a href="{{ url('/horizontal/blade-loops') }}" aria-expanded="false">
                        <i class="mdi mdi-widgets"></i>
                        <span class="hide-menu">Blade Loops </span>
                    </a>
                </li>

                <li>
                    <a class="has-arrow " href="#" aria-expanded="false">
                        <i class="mdi mdi-book-multiple"></i>
                        <span class="hide-menu">Layouts</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('horizontal.demos.boxed') }}">Boxed</a></li>
                        <li><a href="{{ route('horizontal.demos.logocenter') }}">Logo Center</a></li>
                        <li><a href="{{ route('horizontal.demos.singlecolumn') }}">Single Column</a></li>
                        <li><a href="{{ route('horizontal.demos.fixheader') }}">Fix header</a></li>
                        <li><a href="{{ route('horizontal.demos.fixsidebar') }}">Fix sidebar</a></li>
                        <li><a href="{{ route('horizontal.demos.fixheadersidebar') }}">Fix header & Sidebar</a></li>
                    </ul>
                </li>

                @inject('menuItems', 'sidebarmenuservice')

                @include('templates.application.components.menu-items-vertical',
                [ 'items' => $menuItems->getMenuItems('horizontal') , 'template' => 'horizontal'])

            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->