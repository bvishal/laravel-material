<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * Authentication Routes
 */
Auth::routes();


/*
 * When visiting root, redirect to first home
 */
Route::view('/','templates.landing.index');
Route::redirect('/home', '/material/home-one', 302);


/*
 * Demos for "material" preset
 */
Route::redirect('/material', '/material/php-to-js-vars', 302);
Route::get('/material/home-one', 'MaterialController@index')->name('material.home-one');
Route::get('/material/home-two', 'MaterialController@hometwo')->name('material.home-two');
Route::get('/material/php-to-js-vars', 'MaterialController@PHPToJSVars')->name('material.demos.phptojsvars');
Route::get('/material/blade-components', 'MaterialController@bladeComponents')->name('material.blade-components');
Route::get('/material/blade-loops', 'MaterialController@bladeLoops')->name('material.blade-loops');
Route::get('/material/demos/boxed', 'MaterialController@boxed')->name('material.demos.boxed');
Route::get('/material/demos/logo-center', 'MaterialController@logoCenter')->name('material.demos.logocenter');
Route::get('/material/demos/single-column', 'MaterialController@singleColumn')->name('material.demos.singlecolumn');
Route::get('/material/demos/fix-header', 'MaterialController@fixHeader')->name('material.demos.fixheader');
Route::get('/material/demos/fix-sidebar', 'MaterialController@fixSidebar')->name('material.demos.fixsidebar');
Route::get('/material/demos/fix-header-sidebar', 'MaterialController@fixHeaderSidebar')->name('material.demos.fixheadersidebar');
Route::get('/material/demos/{group}/{page}','MaterialController@groupPage')->name('material.group.page');

/*
 * Demos for "material-rtl" preset
 */
Route::redirect('/material-rtl', '/material-rtl/php-to-js-vars', 302);
Route::get('/material-rtl/home-one', 'MaterialRtlController@index')->name('material-rtl.home-one');
Route::get('/material-rtl/home-two', 'MaterialRtlController@hometwo')->name('material-rtl.home-two');
Route::get('/material-rtl/php-to-js-vars', 'MaterialRtlController@PHPToJSVars')->name('material-rtl.demos.phptojsvars');
Route::get('/material-rtl/blade-components', 'MaterialRtlController@bladeComponents')->name('material-rtl.blade-components');
Route::get('/material-rtl/blade-loops', 'MaterialRtlController@bladeLoops')->name('material-rtl.blade-loops');
Route::get('/material-rtl/demos/boxed', 'MaterialRtlController@boxed')->name('material-rtl.demos.boxed');
Route::get('/material-rtl/demos/logo-center', 'MaterialRtlController@logoCenter')->name('material-rtl.demos.logocenter');
Route::get('/material-rtl/demos/single-column', 'MaterialRtlController@singleColumn')->name('material-rtl.demos.singlecolumn');
Route::get('/material-rtl/demos/fix-header', 'MaterialRtlController@fixHeader')->name('material-rtl.demos.fixheader');
Route::get('/material-rtl/demos/fix-sidebar', 'MaterialRtlController@fixSidebar')->name('material-rtl.demos.fixsidebar');
Route::get('/material-rtl/demos/fix-header-sidebar', 'MaterialRtlController@fixHeaderSidebar')->name('material-rtl.demos.fixheadersidebar');
Route::get('/material-rtl/demos/{group}/{page}','MaterialRtlController@groupPage')->name('material-rtl.group.page');

/*
 * Demos for "minisidebar" preset
 */
Route::redirect('/minisidebar', '/minisidebar/php-to-js-vars', 302);
Route::get('/minisidebar/home-one', 'MinisidebarController@index')->name('minisidebar.home-one');
Route::get('/minisidebar/home-two', 'MinisidebarController@hometwo')->name('minisidebar.home-two');
Route::get('/minisidebar/php-to-js-vars', 'MinisidebarController@PHPToJSVars')->name('minisidebar.demos.phptojsvars');
Route::get('/minisidebar/blade-components', 'MinisidebarController@bladeComponents')->name('minisidebar.blade-components');
Route::get('/minisidebar/blade-loops', 'MinisidebarController@bladeLoops')->name('minisidebar.blade-loops');
Route::get('/minisidebar/demos/boxed', 'MinisidebarController@boxed')->name('minisidebar.demos.boxed');
Route::get('/minisidebar/demos/logo-center', 'MinisidebarController@logoCenter')->name('minisidebar.demos.logocenter');
Route::get('/minisidebar/demos/single-column', 'MinisidebarController@singleColumn')->name('minisidebar.demos.singlecolumn');
Route::get('/minisidebar/demos/fix-header', 'MinisidebarController@fixHeader')->name('minisidebar.demos.fixheader');
Route::get('/minisidebar/demos/fix-sidebar', 'MinisidebarController@fixSidebar')->name('minisidebar.demos.fixsidebar');
Route::get('/minisidebar/demos/fix-header-sidebar', 'MinisidebarController@fixHeaderSidebar')->name('minisidebar.demos.fixheadersidebar');
Route::get('/minisidebar/demos/{group}/{page}','MinisidebarController@groupPage')->name('minisidebar.group.page');

/*
 * Demos for "horizontal" preset
 */
Route::redirect('/horizontal', '/horizontal/php-to-js-vars', 302);
Route::get('/horizontal/home-one', 'HorizontalController@index')->name('horizontal.home-one');
Route::get('/horizontal/home-two', 'HorizontalController@hometwo')->name('horizontal.home-two');
Route::get('/horizontal/php-to-js-vars', 'HorizontalController@PHPToJSVars')->name('horizontal.demos.phptojsvars');
Route::get('/horizontal/blade-components', 'HorizontalController@bladeComponents')->name('horizontal.blade-components');
Route::get('/horizontal/blade-loops', 'HorizontalController@bladeLoops')->name('horizontal.blade-loops');
Route::get('/horizontal/demos/boxed', 'HorizontalController@boxed')->name('horizontal.demos.boxed');
Route::get('/horizontal/demos/logo-center', 'HorizontalController@logoCenter')->name('horizontal.demos.logocenter');
Route::get('/horizontal/demos/single-column', 'HorizontalController@singleColumn')->name('horizontal.demos.singlecolumn');
Route::get('/horizontal/demos/fix-header', 'HorizontalController@fixHeader')->name('horizontal.demos.fixheader');
Route::get('/horizontal/demos/fix-sidebar', 'HorizontalController@fixSidebar')->name('horizontal.demos.fixsidebar');
Route::get('/horizontal/demos/fix-header-sidebar', 'HorizontalController@fixHeaderSidebar')->name('horizontal.demos.fixheadersidebar');
Route::get('/horizontal/demos/{group}/{page}','HorizontalController@groupPage')->name('horizontal.group.page');

/*
 * Demos for "dark" preset
 */
Route::redirect('/dark', '/dark/php-to-js-vars', 302);
Route::get('/dark/home-one', 'DarkController@index')->name('dark.home-one');
Route::get('/dark/home-two', 'DarkController@hometwo')->name('dark.home-two');
Route::get('/dark/php-to-js-vars', 'DarkController@PHPToJSVars')->name('dark.demos.phptojsvars');
Route::get('/dark/blade-components', 'DarkController@bladeComponents')->name('dark.blade-components');
Route::get('/dark/blade-loops', 'DarkController@bladeLoops')->name('dark.blade-loops');
Route::get('/dark/demos/boxed', 'DarkController@boxed')->name('dark.demos.boxed');
Route::get('/dark/demos/logo-center', 'DarkController@logoCenter')->name('dark.demos.logocenter');
Route::get('/dark/demos/single-column', 'DarkController@singleColumn')->name('dark.demos.singlecolumn');
Route::get('/dark/demos/fix-header', 'DarkController@fixHeader')->name('dark.demos.fixheader');
Route::get('/dark/demos/fix-sidebar', 'DarkController@fixSidebar')->name('dark.demos.fixsidebar');
Route::get('/dark/demos/fix-header-sidebar', 'DarkController@fixHeaderSidebar')->name('dark.demos.fixheadersidebar');
Route::get('/dark/demos/{group}/{page}','DarkController@groupPage')->name('dark.group.page');