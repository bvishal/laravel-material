let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const sassOptions = {
    precision: 5
};

/* ==============================================================
 CORE STYLESHEETS -->
============================================================== */

const resourceFolder = `resources/assets/vendor/wrappixel/material-pro/4.1.0`;

mix
    //Templates
    .sass(`${resourceFolder}/material/scss/style.scss`,       'public/css/material/style.css', sassOptions)
    .sass(`${resourceFolder}/minisidebar/scss/style.scss`,    'public/css/minisidebar/style.css', sassOptions)
    .sass(`${resourceFolder}/horizontal/scss/style.scss`,     'public/css/horizontal/style.css', sassOptions)
    .sass(`${resourceFolder}/dark/scss/style.scss`,           'public/css/dark/style.css', sassOptions)
    .sass(`${resourceFolder}/material-rtl/scss/style.scss`,   'public/css/material-rtl/style.css', sassOptions)

    //Color Palettes
    .sass(`${resourceFolder}/material/scss/colors/blue.scss`,           'public/css/colors', sassOptions)
    .sass(`${resourceFolder}/material/scss/colors/blue-dark.scss`,      'public/css/colors', sassOptions)
    .sass(`${resourceFolder}/material/scss/colors/default.scss`,        'public/css/colors', sassOptions)
    .sass(`${resourceFolder}/material/scss/colors/default-dark.scss`,   'public/css/colors', sassOptions)
    .sass(`${resourceFolder}/material/scss/colors/green.scss`,          'public/css/colors', sassOptions)
    .sass(`${resourceFolder}/material/scss/colors/green-dark.scss`,     'public/css/colors', sassOptions)
    .sass(`${resourceFolder}/material/scss/colors/megna.scss`,          'public/css/colors', sassOptions)
    .sass(`${resourceFolder}/material/scss/colors/megna-dark.scss`,     'public/css/colors', sassOptions)
    .sass(`${resourceFolder}/material/scss/colors/purple.scss`,         'public/css/colors', sassOptions)
    .sass(`${resourceFolder}/material/scss/colors/purple-dark.scss`,    'public/css/colors', sassOptions)
    .sass(`${resourceFolder}/material/scss/colors/red.scss`,            'public/css/colors', sassOptions)
    .sass(`${resourceFolder}/material/scss/colors/red-dark.scss`,       'public/css/colors', sassOptions)
;

/* ==============================================================
END CORE STYLESHEETS -->
============================================================== */

mix.version();
