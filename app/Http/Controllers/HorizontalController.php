<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\DashboardService;
use App\Services\ComponentsService;
use App\Services\BladeViewService;

class HorizontalController extends Controller
{
    public function index()
    {
        return view('demo-apps.horizontal.home-one');
    }

    public function hometwo()
    {
        return view('demo-apps.horizontal.home-two');
    }

    public function PHPToJSVars(DashboardService $dashboardService)
    {
        $chart2Data = $dashboardService->getChart2Data();
        $chartVisitorsData = $dashboardService->getChartVisitorsData();
        $chartCampaignData = $dashboardService->getChartCampaignData();

        return view('demo-apps.horizontal.php-to-js-vars')
            ->with('chart2Data', $chart2Data)
            ->with('chartCampaignData', $chartCampaignData)
            ->with('chartVisitorsData', $chartVisitorsData);
    }

    public function bladeComponents(ComponentsService $componentsService)
    {
        return view('demo-apps.horizontal.blade-components')->with('tabs', $componentsService->getTabs());
    }

    public function bladeLoops(BladeViewService $bladeViewService, ComponentsService $componentsService)
    {
        return view('demo-apps.horizontal.blade-loops')
            ->with('data', $bladeViewService->getData())
            ->with('tabs', $componentsService->getTabs());
    }

    public function boxed()
    {
        return view('demo-apps.horizontal.home-boxed');
    }

    public function logoCenter()
    {
        return view('demo-apps.horizontal.logo-center');
    }

    public function singleColumn()
    {
        return view('demo-apps.horizontal.single-column');
    }

    public function fixHeader()
    {
        return view('demo-apps.horizontal.fix-header');
    }

    public function fixSidebar()
    {
        return view('demo-apps.horizontal.fix-sidebar');
    }

    public function fixHeaderSidebar()
    {
        return view('demo-apps.horizontal.fix-header-sidebar');
    }

    public function groupPage($group, $page)
    {
        return view('demo-apps.horizontal.group-page')
            ->withGroup($group)
            ->withPage($page);
    }
}
