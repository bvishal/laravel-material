<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\BladeViewService;
use App\Services\DashboardService;
use App\Services\ComponentsService;

class MaterialRtlController extends Controller
{
    public function index()
    {
        return view('demo-apps.material-rtl.home-one');
    }

    public function hometwo()
    {
        return view('demo-apps.material-rtl.home-two');
    }

    public function PHPToJSVars(DashboardService $dashboardService)
    {
        $chart2Data = $dashboardService->getChart2Data();
        $chartVisitorsData = $dashboardService->getChartVisitorsData();
        $chartCampaignData = $dashboardService->getChartCampaignData();

        return view('demo-apps.material-rtl.php-to-js-vars')
            ->with('chart2Data', $chart2Data)
            ->with('chartCampaignData', $chartCampaignData)
            ->with('chartVisitorsData', $chartVisitorsData);
    }

    public function bladeComponents(ComponentsService $componentsService)
    {
        return view('demo-apps.material-rtl.blade-components')->with('tabs', $componentsService->getTabs());
    }

    public function bladeLoops(BladeViewService $bladeViewService, ComponentsService $componentsService)
    {
        return view('demo-apps.material-rtl.blade-loops')
            ->with('data', $bladeViewService->getData())
            ->with('tabs', $componentsService->getTabs());
    }

    public function boxed()
    {
        return view('demo-apps.material-rtl.home-boxed');
    }

    public function logoCenter()
    {
        return view('demo-apps.material-rtl.logo-center');
    }

    public function singleColumn()
    {
        return view('demo-apps.material-rtl.single-column');
    }

    public function fixHeader()
    {
        return view('demo-apps.material-rtl.fix-header');
    }

    public function fixSidebar()
    {
        return view('demo-apps.material-rtl.fix-sidebar');
    }

    public function fixHeaderSidebar()
    {
        return view('demo-apps.material-rtl.fix-header-sidebar');
    }

    public function groupPage($group, $page)
    {
        return view('demo-apps.material-rtl.group-page')
            ->withGroup($group)
            ->withPage($page);
    }
}
