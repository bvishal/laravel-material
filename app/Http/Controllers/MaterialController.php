<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\DashboardService;
use App\Services\BladeViewService;
use App\Services\ComponentsService;

class MaterialController extends Controller
{
    public function index()
    {
        return view('demo-apps.material.home-one');
    }

    public function hometwo()
    {
        return view('demo-apps.material.home-two');
    }

    /**
     * Simulating a connection to a database/webservice, to
     * fetch data, putting it in Javascript/Client-Side
     * So then it will be rendered in the browser
     *
     */
    public function PHPToJSVars(DashboardService $dashboardService)
    {
        //Fetching data
        $chart2Data = $dashboardService->getChart2Data();
        $chartVisitorsData = $dashboardService->getChartVisitorsData();
        $chartCampaignData = $dashboardService->getChartCampaignData();

        //Passing data to the view
        return view('demo-apps.material.php-to-js-vars')
            ->with('chart2Data', $chart2Data)
            ->with('chartCampaignData', $chartCampaignData)
            ->with('chartVisitorsData', $chartVisitorsData);
    }

    public function bladeComponents(ComponentsService $componentsService)
    {
        return view('demo-apps.material.blade-components')->with('tabs', $componentsService->getTabs());
    }

    public function bladeLoops(BladeViewService $bladeViewService, ComponentsService $componentsService)
    {
        return view('demo-apps.material.blade-loops')
            ->with('data', $bladeViewService->getData())
            ->with('tabs', $componentsService->getTabs());
    }

    public function boxed()
    {
        return view('demo-apps.material.home-boxed');
    }

    public function logoCenter()
    {
        return view('demo-apps.material.logo-center');
    }

    public function singleColumn()
    {
        return view('demo-apps.material.single-column');
    }

    public function fixHeader()
    {
        return view('demo-apps.material.fix-header');
    }

    public function fixSidebar()
    {
        return view('demo-apps.material.fix-sidebar');
    }

    public function fixHeaderSidebar()
    {
        return view('demo-apps.material.fix-header-sidebar');
    }

    public function groupPage($group, $page)
    {
        return view('demo-apps.material.group-page')
            ->withGroup($group)
            ->withPage($page);
    }
}
